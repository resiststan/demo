package net.resiststan.dao.base;

import java.util.List;

public interface BaseCrud<T, PK> {
    T save(T t) throws DaoException;

    T find(PK pk) throws DaoException;

    List<T> getAll() throws DaoException;

    void update(PK pk, T t) throws DaoException;

    void delete(PK pk) throws DaoException;
}
