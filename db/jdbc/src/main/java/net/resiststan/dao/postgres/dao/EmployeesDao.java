package net.resiststan.dao.postgres.dao;

import net.resiststan.dao.base.BaseCrud;
import net.resiststan.dao.base.BaseDao;
import net.resiststan.dao.base.DaoException;
import net.resiststan.dao.base.Identifier;
import net.resiststan.dao.postgres.PostgresDDL;
import net.resiststan.dao.postgres.PostgresDaoFactory;
import net.resiststan.dao.postgres.PostgresSQLUtil;
import net.resiststan.entity.Department;
import net.resiststan.entity.Employee;
import net.resiststan.entity.Position;
import net.resiststan.entity.Task;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import static net.resiststan.dao.postgres.PostgresDDL.EMPLOYEES_TABLE.COLUMNS.DEPARTMENT_ID;
import static net.resiststan.dao.postgres.PostgresDDL.EMPLOYEES_TABLE.COLUMNS.FIRST_NAME;
import static net.resiststan.dao.postgres.PostgresDDL.EMPLOYEES_TABLE.COLUMNS.ID;
import static net.resiststan.dao.postgres.PostgresDDL.EMPLOYEES_TABLE.COLUMNS.LAST_NAME;
import static net.resiststan.dao.postgres.PostgresDDL.EMPLOYEES_TABLE.COLUMNS.POSITION_ID;
import static net.resiststan.dao.postgres.PostgresDDL.EMPLOYEES_TABLE.TABLE_NAME;

public class EmployeesDao extends BaseDao<Employee, Long> implements PostgresSQLUtil {

    public EmployeesDao() {
        super(null);
    }

    @Override
    protected String getInsertOneQuery() {
        return String.format("INSERT INTO %s (%s, %s, %s, %s) VALUES (?, ?, ?, ?)",
                TABLE_NAME, FIRST_NAME, LAST_NAME, DEPARTMENT_ID, POSITION_ID);
    }

    @Override
    protected String getSelectByPrimaryKeyQuery() {
        return selectBySql(TABLE_NAME, ID);
    }

    @Override
    protected String getSelectAllQuery() {
        return selectAllSql(TABLE_NAME);
    }

    @Override
    protected String getUpdateByPrimaryKeyQuery() {
        return String.format(" UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ? WHERE %s = ?",
                TABLE_NAME, FIRST_NAME, LAST_NAME, DEPARTMENT_ID, POSITION_ID, ID);
    }

    @Override
    protected String getDeleteByPrimaryKeyQuery() {
        return deleteSql(TABLE_NAME, ID);
    }

    @Override
    protected String getLastInsertPK() {
        return getLastIdSql(TABLE_NAME, ID);
    }

    @Override
    protected List<Employee> parseResultSet(ResultSet rs) throws DaoException {
        List<Employee> result = new LinkedList<>();
        try {
            while (rs.next()) {
                Employee employee = new Employee();
                employee.setId(rs.getLong(ID));
                employee.setFirstName(rs.getString(FIRST_NAME));
                employee.setLastName(rs.getString(LAST_NAME));
                employee.setDepartment(getDepartmentById(rs.getLong(DEPARTMENT_ID)));
                employee.setPosition(getPositionById(rs.getLong(POSITION_ID)));
                employee.setTasks(findAllEmployeesTasks(rs.getLong(ID)));

                result.add(employee);
            }
        } catch (Exception e) {
            throw new DaoException(e);
        }
        return result;
    }

    private Department getDepartmentById(Long departmentId) throws DaoException {
        BaseCrud<Department, Long> departmentsDao = getRelationDao(Department.class);
        return departmentsDao.find(departmentId);
    }

    private Position getPositionById(Long positionId) throws DaoException {
        BaseCrud<Position, Long> positionsDao = getRelationDao(Position.class);
        return positionsDao.find(positionId);
    }

    private Set<Task> findAllEmployeesTasks(Long employeeId) throws DaoException {
        return Set.copyOf(getAllTasks(employeeId));
    }

    @Deprecated
    private List<Task> getAllTasks(Long pk) throws DaoException {
        List<Task> list;
        String sql = String.format("SELECT * FROM %s WHERE %s = ?",
                PostgresDDL.EMPLOYEES_TASKS_TABLE.TABLE_NAME, PostgresDDL.EMPLOYEES_TASKS_TABLE.COLUMNS.EMPLOYEE_ID);

        try (PreparedStatement statement = getConnection().prepareStatement(sql)) {
            statement.setObject(1, pk);
            ResultSet rs = statement.executeQuery();
            list = parseResultSetTask(rs);
        } catch (Exception e) {
            throw new DaoException(e);
        }

        return list;
    }

    @Deprecated
    private List<Task> parseResultSetTask(ResultSet rs) throws DaoException {
        List<Task> result = new LinkedList<>();
        try {
            while (rs.next()) {
                Task task = new Task();
                task.setId(rs.getLong(PostgresDDL.TASKS_TABLE.COLUMNS.ID));
                task.setDescription(rs.getString(PostgresDDL.TASKS_TABLE.COLUMNS.DESCRIPTION));
                result.add(task);
            }
        } catch (Exception e) {
            throw new DaoException(e);
        }
        return result;
    }

    private BaseCrud getRelationDao(Class entityClass) throws DaoException {
        return new PostgresDaoFactory().getDao(entityClass, getConnection());
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Employee employee) throws DaoException {
        try {
            statement.setString(1, employee.getFirstName());
            statement.setString(2, employee.getLastName());
            statement.setLong(3, getIdentifierValue(employee.getDepartment()));
            statement.setLong(4, getIdentifierValue(employee.getPosition()));

            insertEmployeesTasks(employee.getTasks(), employee.getId());
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    private void insertEmployeesTasks(Set<Task> tasks, Long employeeId) throws DaoException {
//        insertTasks(tasks, employeeId);
    }

    @Deprecated
    private void insertTasks(Set<Task> tasks, Long employeeId) throws DaoException {
        Iterator<Task> iterator = tasks.iterator();

        while (iterator.hasNext()) {
            Long taskId = iterator.next().getId();

            String sql = String.format("INSERT INTO %s (%s, %s) VALUES (?, ?)",
                    PostgresDDL.EMPLOYEES_TASKS_TABLE.TABLE_NAME,
                    PostgresDDL.EMPLOYEES_TASKS_TABLE.COLUMNS.TASK_ID,
                    PostgresDDL.EMPLOYEES_TASKS_TABLE.COLUMNS.EMPLOYEE_ID);

            try (PreparedStatement statement = getConnection().prepareStatement(sql)) {
                addTaskEmployee(statement, taskId, employeeId);
                int count = statement.executeUpdate();

                if (count != 1) {
                    throw new DaoException("On persist modify more then 1 record: " + count);
                }
            } catch (Exception e) {
                throw new DaoException(e);
            }
        }
    }

    @Deprecated
    private void addTaskEmployee(PreparedStatement statement, Long taskId, Long employeeId) throws DaoException {
        try {
            statement.setLong(1, taskId);
            statement.setLong(2, employeeId);
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Employee employee, Long id) throws DaoException {
        try {
            updateEmployeesTasks(employee.getTasks());

            statement.setString(1, employee.getFirstName());
            statement.setString(2, employee.getLastName());
            statement.setLong(3, getIdentifierValue(employee.getDepartment()));
            statement.setLong(4, getIdentifierValue(employee.getPosition()));
            statement.setLong(5, id);
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    private void updateEmployeesTasks(Set<Task> tasks) {
        updateTasks(tasks);
    }

    @Deprecated
    private void updateTasks(Set<Task> tasks) {

    }

    private Long getIdentifierValue(Identifier<Long> identifier) {
        return validateIdentifier(identifier) ? identifier.getId() : -1L;
    }

    private boolean validateIdentifier(Identifier identifier) {
        return identifier != null && identifier.getId() != null;
    }
}
