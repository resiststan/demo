package net.resiststan.dao.postgres.dao;

import net.resiststan.dao.base.BaseDao;
import net.resiststan.dao.base.DaoException;
import net.resiststan.dao.postgres.PostgresSQLUtil;
import net.resiststan.entity.Position;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

import static net.resiststan.dao.postgres.PostgresDDL.POSITIONS_TABLE.COLUMNS.ID;
import static net.resiststan.dao.postgres.PostgresDDL.POSITIONS_TABLE.COLUMNS.NAME;
import static net.resiststan.dao.postgres.PostgresDDL.POSITIONS_TABLE.TABLE_NAME;

public class PositionsDao extends BaseDao<Position, Long> implements PostgresSQLUtil {

    public PositionsDao() {
        super(null);
    }

    @Override
    protected String getInsertOneQuery() {
        return String.format("INSERT INTO %s (%s) VALUES (?)", TABLE_NAME, NAME);
    }

    @Override
    protected String getSelectByPrimaryKeyQuery() {
        return selectBySql(TABLE_NAME, ID);
    }

    @Override
    protected String getSelectAllQuery() {
        return selectAllSql(TABLE_NAME);
    }

    @Override
    protected String getUpdateByPrimaryKeyQuery() {
        return String.format(" UPDATE %s SET %s = ? WHERE %s = ?", TABLE_NAME, NAME, ID);
    }

    @Override
    protected String getDeleteByPrimaryKeyQuery() {
        return deleteSql(TABLE_NAME, ID);
    }

    @Override
    protected String getLastInsertPK() {
        return getLastIdSql(TABLE_NAME, ID);
    }

    @Override
    protected List<Position> parseResultSet(ResultSet rs) throws DaoException {
        List<Position> result = new LinkedList<>();
        try {
            while (rs.next()) {
                Position position = new Position();
                position.setId(rs.getLong(ID));
                position.setName(rs.getString(NAME));
                result.add(position);
            }
        } catch (Exception e) {
            throw new DaoException(e);
        }
        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Position position) throws DaoException {
        try {
            statement.setString(1, position.getName());
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Position position, Long id) throws DaoException {
        try {
            statement.setString(1, position.getName());
            statement.setLong(3, id);
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }
}
