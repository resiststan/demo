package net.resiststan.dao.base;

import java.sql.Connection;

public interface DaoFactory {
    BaseCrud getDao(Class dtoClass, Connection connection) throws DaoException;
}
