package net.resiststan.dao.postgres.dao;

import net.resiststan.dao.base.BaseDao;
import net.resiststan.dao.base.DaoException;
import net.resiststan.dao.postgres.PostgresSQLUtil;
import net.resiststan.entity.Task;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

import static net.resiststan.dao.postgres.PostgresDDL.TASKS_TABLE.COLUMNS.DESCRIPTION;
import static net.resiststan.dao.postgres.PostgresDDL.TASKS_TABLE.COLUMNS.ID;
import static net.resiststan.dao.postgres.PostgresDDL.TASKS_TABLE.TABLE_NAME;

public class TasksDao extends BaseDao<Task, Long> implements PostgresSQLUtil {

    public TasksDao() {
        super(null);
    }

    @Override
    protected String getInsertOneQuery() {
        return String.format("INSERT INTO %s (%s) VALUES (?)", TABLE_NAME, DESCRIPTION);
    }

    @Override
    protected String getSelectByPrimaryKeyQuery() {
        return selectBySql(TABLE_NAME, ID);
    }

    @Override
    protected String getSelectAllQuery() {
        return selectAllSql(TABLE_NAME);
    }

    @Override
    protected String getUpdateByPrimaryKeyQuery() {
        return String.format(" UPDATE %s SET %s = ? WHERE %s = ?", TABLE_NAME, DESCRIPTION, ID);
    }

    @Override
    protected String getDeleteByPrimaryKeyQuery() {
        return deleteSql(TABLE_NAME, ID);
    }

    @Override
    protected String getLastInsertPK() {
        return getLastIdSql(TABLE_NAME, ID);
    }

    @Override
    protected List<Task> parseResultSet(ResultSet rs) throws DaoException {
        List<Task> result = new LinkedList<>();
        try {
            while (rs.next()) {
                Task task = new Task();
                task.setId(rs.getLong(ID));
                task.setDescription(rs.getString(DESCRIPTION));
                result.add(task);
            }
        } catch (Exception e) {
            throw new DaoException(e);
        }
        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Task task) throws DaoException {
        try {
            statement.setString(1, task.getDescription());
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Task task, Long id) throws DaoException {
        try {
            statement.setString(1, task.getDescription());
            statement.setLong(3, id);
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }
}
