package net.resiststan.dao.base;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Logger;

public abstract class DBData {
    protected final Logger LOG = Logger.getLogger(DBData.class.getName());

    private final DataSource dataSource;

    public DBData(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void create() {
        try (Connection connection = dataSource.getConnection()) {
            create(connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected abstract void create(Connection connection);
}
