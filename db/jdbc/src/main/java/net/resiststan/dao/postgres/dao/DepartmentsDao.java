package net.resiststan.dao.postgres.dao;

import net.resiststan.dao.base.BaseDao;
import net.resiststan.dao.base.DaoException;
import net.resiststan.dao.postgres.PostgresSQLUtil;
import net.resiststan.entity.Department;
import net.resiststan.entity.Location;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

import static net.resiststan.dao.postgres.PostgresDDL.DEPARTMENTS_TABLE.COLUMNS.DESCRIPTION;
import static net.resiststan.dao.postgres.PostgresDDL.DEPARTMENTS_TABLE.COLUMNS.ID;
import static net.resiststan.dao.postgres.PostgresDDL.DEPARTMENTS_TABLE.COLUMNS.LOCATION;
import static net.resiststan.dao.postgres.PostgresDDL.DEPARTMENTS_TABLE.COLUMNS.NAME;
import static net.resiststan.dao.postgres.PostgresDDL.DEPARTMENTS_TABLE.TABLE_NAME;

public class DepartmentsDao extends BaseDao<Department, Long> implements PostgresSQLUtil {

    public DepartmentsDao() {
        super(null);
    }

    @Override
    protected String getInsertOneQuery() {
        return String.format("INSERT INTO %s (%s, %s, %s) VALUES (?, ?, ?)", TABLE_NAME, NAME, DESCRIPTION, LOCATION);
    }

    @Override
    protected String getSelectByPrimaryKeyQuery() {
        return selectBySql(TABLE_NAME, ID);
    }

    @Override
    protected String getSelectAllQuery() {
        return selectAllSql(TABLE_NAME);
    }

    @Override
    protected String getUpdateByPrimaryKeyQuery() {
        return String.format(" UPDATE %s SET %s = ?, %s = ?, %s = ? WHERE %s = ?", TABLE_NAME, NAME, DESCRIPTION, LOCATION, ID);
    }

    @Override
    protected String getDeleteByPrimaryKeyQuery() {
        return deleteSql(TABLE_NAME, ID);
    }

    @Override
    protected String getLastInsertPK() {
        return getLastIdSql(TABLE_NAME, ID);
    }

    @Override
    protected List<Department> parseResultSet(ResultSet rs) throws DaoException {
        List<Department> result = new LinkedList<>();
        try {
            while (rs.next()) {
                Department department = new Department();
                department.setId(rs.getLong(ID));
                department.setName(rs.getString(NAME));
                department.setDescription(rs.getString(DESCRIPTION));
                department.setLocation(locationFrom(rs.getString(LOCATION)));
                result.add(department);
            }
        } catch (Exception e) {
            throw new DaoException(e);
        }
        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Department department) throws DaoException {
        try {
            statement.setString(1, department.getName());
            statement.setString(2, department.getDescription());
            statement.setString(3, stringFrom(department.getLocation()));
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Department department, Long id) throws DaoException {
        try {
            statement.setString(1, department.getName());
            statement.setString(2, department.getDescription());
            statement.setString(3, stringFrom(department.getLocation()));
            statement.setLong(4, id);
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    private String stringFrom(Location location) {
        Location.Angle longitude = location.getLongitude();
        Location.Angle latitude = location.getLatitude();
        return String.format("%s,%s;%s,%s", latitude.getD1(), latitude.getD2(), longitude.getD1(), longitude.getD2());
    }

    private Location locationFrom(String string) {
        if (!string.isEmpty()) {
            Pattern pattern = Pattern.compile("\\D");
            String[] strings = pattern.split(string, 4);

            if (strings.length == 4) {
                try {
                    Location.Angle latitude = new Location.Angle(Integer.parseInt(strings[0]), Integer.parseInt(strings[1]));
                    Location.Angle longitude = new Location.Angle(Integer.parseInt(strings[2]), Integer.parseInt(strings[3]));

                    return new Location(latitude, longitude);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
        }

        return null;
    }
}
