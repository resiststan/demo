package net.resiststan.dao.postgres;

import net.resiststan.dao.base.BaseCrud;
import net.resiststan.dao.base.DaoException;
import net.resiststan.dao.base.DaoFactory;
import net.resiststan.entity.Department;
import net.resiststan.entity.Employee;
import net.resiststan.entity.Location;
import net.resiststan.entity.Position;
import net.resiststan.entity.Task;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class EmployeesDaoTest {
    private static BaseCrud<Employee, Long> employeesDao;
    private static Connection connection;
    private static Savepoint savepoint;
    private static DaoFactory daoFactory;

    private static List<Employee> employees;
    private static List<Location> locations;
    private static List<Position> positions;
    private static List<Department> departments;
    private static List<Task> tasks;

    @BeforeClass
    public static void setUp() throws DaoException, SQLException {
        DataSource dataSource = new PostgresDataSource().getDataSource();
        connection = dataSource.getConnection();
        connection.setAutoCommit(false);
        daoFactory = new PostgresDaoFactory();
        employeesDao = daoFactory.getDao(Employee.class, connection);

        initData();
        savepoint = connection.setSavepoint();
    }

    @AfterClass
    public static void closeConnection() throws SQLException {
        connection.rollback();
        connection.close();
    }

    @After
    public void rollbackAfterAnyTest() throws SQLException {
        connection.rollback(savepoint);
    }

    @Test
    public void createTest() throws DaoException {
        Employee srcEmployee = employees.get(1);
        Employee persistedEmployee = employeesDao.save(srcEmployee);

        assertNotNull("ID is null", persistedEmployee.getId());
        assertEquals(srcEmployee.getFirstName(), persistedEmployee.getFirstName());
        assertEquals(srcEmployee.getLastName(), persistedEmployee.getLastName());
        assertEquals(srcEmployee.getDepartment(), persistedEmployee.getDepartment());
        assertEquals(srcEmployee.getPosition(), persistedEmployee.getPosition());

//        assertEquals(srcEmployee.getTasks().size(), persistedEmployee.getTasks().size());
//        assertTrue(srcEmployee.getTasks().containsAll(persistedEmployee.getTasks()));
    }

    @Test
    public void selectAllTest() throws DaoException {
        int expectEmployeesCount = employees.size();

        for (Employee employee : employees) {
            employeesDao.save(employee);
        }

        List<Employee> employeeList = employeesDao.getAll();

        assertEquals(expectEmployeesCount, employeeList.size());
    }

    @Test
    public void deleteTest() throws DaoException {
        Employee persistedEmployee = employeesDao.save(employees.get(1));

        int expectEmployeesCount = employeesDao.getAll().size() - 1;

        employeesDao.delete(persistedEmployee.getId());

        assertEquals(expectEmployeesCount, employeesDao.getAll().size());
        assertNull(employeesDao.find(persistedEmployee.getId()));
    }

    @Test
    public void findByIdTest() throws DaoException {
        Employee persistedEmployee = employeesDao.save(employees.get(1));

        Employee foundObject = employeesDao.find(persistedEmployee.getId());

        assertNotNull("ID can not be null", foundObject.getId());
        assertEquals(persistedEmployee.getFirstName(), foundObject.getFirstName());
        assertEquals(persistedEmployee.getLastName(), foundObject.getLastName());
        assertEquals(persistedEmployee.getDepartment(), foundObject.getDepartment());
        assertEquals(persistedEmployee.getPosition(), foundObject.getPosition());
        assertEquals(persistedEmployee.getTasks().size(), foundObject.getTasks().size());
        assertTrue(persistedEmployee.getTasks().containsAll(persistedEmployee.getTasks()));
    }

    @Test
    public void updateTest() throws DaoException {
        Employee employeeFromDbBeforeUpdate = employeesDao.save(employees.get(1));

        String expectFirstName = "Fedor";
        employeeFromDbBeforeUpdate.setFirstName(expectFirstName);

        String expectLastName = "Ivanovich";
        employeeFromDbBeforeUpdate.setLastName(expectLastName);

        Department expectDepartment = departments.get(3);
        employeeFromDbBeforeUpdate.setDepartment(expectDepartment);

        Position expectPosition = positions.get(11);
        employeeFromDbBeforeUpdate.setPosition(expectPosition);

        Set<Task> expectTasks = new HashSet<>();
        expectTasks.add(tasks.get(0));
        expectTasks.add(Task.builder().description("work eat rest repeat ").build());
        employeeFromDbBeforeUpdate.setTasks(expectTasks);

        employeesDao.update(employeeFromDbBeforeUpdate.getId(), employeeFromDbBeforeUpdate);

        Employee actualEmployee = employeesDao.find(employeeFromDbBeforeUpdate.getId());

        assertEquals(expectFirstName, actualEmployee.getFirstName());
        assertEquals(expectLastName, actualEmployee.getLastName());
        assertEquals(expectDepartment, actualEmployee.getDepartment());
        assertEquals(expectPosition, actualEmployee.getPosition());

//        assertEquals(expectTasks.size(), actualEmployee.getTasks().size());
//        assertTrue(expectTasks.containsAll(actualEmployee.getTasks()));
    }

    private static void initData() throws DaoException {
        initLocations();
        initDepartments();
        initPositions();
        initTasks();

        employees = Arrays.asList(
                Employee.builder().firstName("Mari").lastName("Ross").position(positions.get(0)).department(departments.get(0)).tasks(Set.copyOf(tasks)).build(),
                Employee.builder().firstName("Nick").lastName("Moss").position(positions.get(1)).department(departments.get(0)).tasks(Set.copyOf(tasks)).build(),
                Employee.builder().firstName("Sho").lastName("Brown").position(positions.get(2)).department(departments.get(0)).tasks(Set.copyOf(tasks)).build(),
                Employee.builder().firstName("Geek").lastName("Break").position(positions.get(3)).department(departments.get(0)).tasks(Set.copyOf(tasks)).build(),

                Employee.builder().firstName("Roma").lastName("Rick").position(positions.get(4)).department(departments.get(1)).tasks(Set.copyOf(tasks)).build(),
                Employee.builder().firstName("Ann").lastName("Bell").position(positions.get(5)).department(departments.get(1)).tasks(Set.copyOf(tasks)).build(),
                Employee.builder().firstName("Fill").lastName("Car").position(positions.get(6)).department(departments.get(1)).tasks(Set.copyOf(tasks)).build(),
                Employee.builder().firstName("Lana").lastName("Don").position(positions.get(7)).department(departments.get(1)).tasks(Set.copyOf(tasks)).build(),

                Employee.builder().firstName("Kira").lastName("Hill").position(positions.get(8)).department(departments.get(2)).tasks(Set.copyOf(tasks)).build(),
                Employee.builder().firstName("Zak").lastName("Ver").position(positions.get(9)).department(departments.get(2)).tasks(Set.copyOf(tasks)).build(),
                Employee.builder().firstName("Lira").lastName("Age").position(positions.get(10)).department(departments.get(2)).tasks(Set.copyOf(tasks)).build(),
                Employee.builder().firstName("John").lastName("Tor").position(positions.get(11)).department(departments.get(2)).tasks(Set.copyOf(tasks)).build(),

                Employee.builder().firstName("Lisa").lastName("Mur").position(positions.get(12)).department(departments.get(3)).tasks(Set.copyOf(tasks)).build(),
                Employee.builder().firstName("Tom").lastName("Rod").position(positions.get(13)).department(departments.get(3)).tasks(Set.copyOf(tasks)).build(),
                Employee.builder().firstName("Lily").lastName("Pain").position(positions.get(14)).department(departments.get(3)).tasks(Set.copyOf(tasks)).build(),
                Employee.builder().firstName("Lom").lastName("Save").position(positions.get(15)).department(departments.get(3)).tasks(Set.copyOf(tasks)).build(),

                Employee.builder().firstName("Ron").lastName("Guns").position(positions.get(16)).department(departments.get(4)).tasks(Set.copyOf(tasks)).build(),
                Employee.builder().firstName("Helga").lastName("Fi").position(positions.get(17)).department(departments.get(4)).tasks(Set.copyOf(tasks)).build(),
                Employee.builder().firstName("Mick").lastName("Ruk").position(positions.get(18)).department(departments.get(4)).tasks(Set.copyOf(tasks)).build(),
                Employee.builder().firstName("Zoi").lastName("Will").position(positions.get(19)).department(departments.get(4)).tasks(Set.copyOf(tasks)).build()
        );
    }

    private static void initLocations() {
        locations = Arrays.asList(
                new Location(new Location.Angle(69, 191466), new Location.Angle(33, 235617)),
                new Location(new Location.Angle(43, 587769), new Location.Angle(39, 736788)),
                new Location(new Location.Angle(54, 722154), new Location.Angle(20, 486398)),
                new Location(new Location.Angle(43, 117312), new Location.Angle(131, 912648))
        );
    }

    private static void initDepartments() throws DaoException {
        departments = Arrays.asList(
                Department.builder().name("Main Department").location(locations.get(2)).description("Main Department").build(),
                Department.builder().name("North Department").location(locations.get(0)).build(),
                Department.builder().name("South Department").location(locations.get(1)).build(),
                Department.builder().name("West Department").location(locations.get(2)).build(),
                Department.builder().name("East Department").location(locations.get(3)).build()
        );

        saveAll(Department.class, departments);
    }

    private static void initPositions() throws DaoException {
        positions = Arrays.asList(
                Position.builder().name("Director of Main Department").build(),
                Position.builder().name("Assistant of Main Department").build(),
                Position.builder().name("Resource allocator of Main Department").build(),
                Position.builder().name("Security officer of Main Department").build(),

                Position.builder().name("Director of North Department").build(),
                Position.builder().name("Assistant of North Department").build(),
                Position.builder().name("Resource allocator of North Department").build(),
                Position.builder().name("Security officer of North Department").build(),

                Position.builder().name("Director of South Department").build(),
                Position.builder().name("Assistant of South Department").build(),
                Position.builder().name("Resource allocator of South Department").build(),
                Position.builder().name("Security officer of South Department").build(),

                Position.builder().name("Director of West Department").build(),
                Position.builder().name("Assistant of West Department").build(),
                Position.builder().name("Resource allocator of West Department").build(),
                Position.builder().name("Security officer of West Department").build(),

                Position.builder().name("Director of East Department").build(),
                Position.builder().name("Assistant of East Department").build(),
                Position.builder().name("Resource allocator of East Department").build(),
                Position.builder().name("Security officer of East Department").build()
        );

        saveAll(Position.class, positions);
    }

    private static void initTasks() throws DaoException {
        tasks = Arrays.asList(
                Task.builder().description("to be on the same wavelength with colleagues").build(),
                Task.builder().description("have a good mood").build(),
                Task.builder().description("make the world a better place").build()
        );

        saveAll(Task.class, tasks);
    }

    private static <T> void saveAll(Class dtoClass, List<T> list) throws DaoException {
        BaseCrud<T, Long> dao = daoFactory.getDao(dtoClass, connection);

        for (int i = 0; i < list.size(); i++) {
            T save = dao.save(list.get(i));
            list.set(i, save);
        }
    }
}
