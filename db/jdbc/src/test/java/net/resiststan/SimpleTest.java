package net.resiststan;

import org.junit.BeforeClass;
import org.junit.Test;

import javax.sql.rowset.CachedRowSet;
import javax.sql.rowset.RowSetFactory;
import javax.sql.rowset.RowSetProvider;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Types;
import java.util.Calendar;

public class SimpleTest {
    private static final String USERNAME = "postgres";
    private static final String PASSWORD = "root";
    private static final String URL_CONNECTION = "jdbc:postgresql://localhost:5432/demo";

    @BeforeClass
    public static void loadDriver() {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void BatchTest() {
        try (
                Connection connection = createConnection();
                Statement statement = connection.createStatement()
        ) {
            connection.setAutoCommit(false);

            createTableUsers(statement);

            statement.addBatch("insert into Users (name, secret) values ('Nick', 'secret 1');");
            statement.addBatch("insert into Users (name, secret) values ('Anna', 'secret 2');");
            statement.addBatch("insert into Users (name, secret) values ('Nina', 'secret 3');");
            statement.addBatch("insert into Users (name, secret) values ('Mark', 'secret 4');");
            statement.addBatch("insert into Users (name, secret) values ('Rosa', 'secret 5');");
            statement.addBatch("insert into Users (name, secret) values ('Ross', 'secret 6');");
            statement.addBatch("insert into Users (name, secret) values ('Lena', 'secret 7');");
            statement.addBatch("insert into Users (name, secret) values ('Mick', 'secret 8');");
            statement.addBatch("insert into Users (name, secret) values ('Luna', 'secret 9');");
            statement.addBatch("insert into Users (name, secret) values ('Tony', 'secret 10');");

            int batchLength = statement.executeBatch().length;

            System.out.println(String.format("Batch length - %s", batchLength));

            dropTableUsers(statement);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.out.println("----------------");
    }

    @Test
    public void SavepointTest() {
        try (
                Connection connection = createConnection();
                Statement statement = connection.createStatement()
        ) {
            connection.setAutoCommit(false);

            createTableUsers(statement);

            int rowCount = getRowCount(statement);
            System.out.println(String.format("In table Users %s rows", rowCount));

            statement.executeUpdate("insert into Users (name, secret) values ('Nick', 'secret 1');");
            statement.executeUpdate("insert into Users (name, secret) values ('Anna', 'secret 2');");
            Savepoint savepoint = connection.setSavepoint();

            statement.executeUpdate("insert into Users (name, secret) values ('Nina', 'secret 3');");

            connection.rollback(savepoint);
            connection.commit();

            rowCount = getRowCount(statement);
            System.out.println(String.format("In table Users %s rows", rowCount));

            dropTableUsers(statement);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.out.println("----------------");
    }

    @Test
    public void updateDataWithCachingRowSet() {
        try (
                Connection connection = createConnection();
                Statement statement = connection.createStatement()
        ) {
            createTableUsersAndPopulate(statement);

            PreparedStatement ps = createScrollablePrepareStatement(connection, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ps.execute();
            ResultSet resultSet = ps.getResultSet();

            RowSetFactory rowSetFactory = RowSetProvider.newFactory();
            CachedRowSet cachedRowSet = rowSetFactory.createCachedRowSet();
            cachedRowSet.populate(resultSet);
            resultSet.close();

            cachedRowSet.absolute(1);
            cachedRowSet.deleteRow();
            cachedRowSet.beforeFirst();

            while (cachedRowSet.next()) {
                printUserData(cachedRowSet);
            }

            connection.setAutoCommit(false);
            cachedRowSet.acceptChanges(connection);

            dropTableUsers(statement);
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.out.println("----------------");
    }

    @Test
    public void DataCachingTest() {
        CachedRowSet cachedRowSet = createCachedRowSet();

        try {
            printAllUsersData(cachedRowSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.out.println("----------------");
    }

    private CachedRowSet createCachedRowSet() {
        try (
                Connection connection = createConnection();
                Statement statement = connection.createStatement()
        ) {
            createTableUsersAndPopulate(statement);

            PreparedStatement ps = createScrollablePrepareStatement(connection, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ps.execute();
            ResultSet resultSet = ps.getResultSet();

            RowSetFactory rowSetFactory = RowSetProvider.newFactory();
            CachedRowSet cachedRowSet = rowSetFactory.createCachedRowSet();
            cachedRowSet.populate(resultSet);
            resultSet.close();

            dropTableUsers(statement);

            return cachedRowSet;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Test
    public void UpdatableScrollResultSetTest() {
        try (
                Connection connection = createConnection();
                Statement statement = connection.createStatement()
        ) {
            createTableUsersAndPopulate(statement);

            PreparedStatement ps = createScrollablePrepareStatement(connection, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ps.execute();
            ResultSet resultSet = ps.getResultSet();

            System.out.println(" -- Before update -- ");
            while (resultSet.next()) {
                printUserData(resultSet);
            }

            resultSet.last();
            resultSet.updateString("secret", "no secrets");
            resultSet.updateRow();

            resultSet.moveToInsertRow();
            resultSet.updateString("name", "Clark");
            resultSet.updateString("secret", "***");
            resultSet.insertRow();

            resultSet.first();
            resultSet.deleteRow();

            System.out.println(" -- After update -- ");
            resultSet.beforeFirst();
            printAllUsersData(resultSet);

            ps.close();
            dropTableUsers(statement);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.out.println("----------------");
    }

    @Test
    public void ScrollResultSetTest() {
        try (
                Connection connection = createConnection();
                Statement statement = connection.createStatement()
        ) {
            createTableUsersAndPopulate(statement);

            PreparedStatement ps = createScrollablePrepareStatement(connection, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ps.execute();
            ResultSet resultSet = ps.getResultSet();

            if (resultSet.relative(2)) {
                printUserData(resultSet);
            }

            if (resultSet.previous()) {
                printUserData(resultSet);
            }

            if (resultSet.absolute(3)) {
                printUserData(resultSet);
            }

            ps.close();
            dropTableUsers(statement);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.out.println("----------------");
    }

    private PreparedStatement createScrollablePrepareStatement(Connection connection, int scrollType, int Concur) throws SQLException {
        PreparedStatement ps = connection.prepareStatement("SELECT * FROM Users", ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ps.execute();

        return ps;
    }

    @Test
    public void FunctionTest() {
        try (
                Connection connection = createConnection();
                Statement statement = connection.createStatement()
        ) {
            createTableUsersAndPopulate(statement);

            String procSQL = "CREATE OR REPLACE FUNCTION getUsers(mycurs OUT refcursor) "
                    + " RETURNS refcursor "
                    + " AS $$ "
                    + " BEGIN "
                    + "     OPEN mycurs FOR SELECT name, LENGTH(secret) as secret_length FROM Users WHERE LENGTH(secret)>=23; "
                    + " END; "
                    + " $$ "
                    + " LANGUAGE plpgsql";

            //We must be inside a transaction for cursors to work.
            connection.setAutoCommit(false);
            statement.execute(procSQL);

            CallableStatement callableStatement = connection.prepareCall("{? = call user_count_with_long_secret()}");
            callableStatement.registerOutParameter(1, Types.REF_CURSOR);
            callableStatement.execute();

            System.out.println("User with secret length more 23...");

            ResultSet resultSet = (ResultSet) callableStatement.getObject(1);
            while (resultSet.next()) {
                String name = resultSet.getString("name");
                int secretLength = resultSet.getInt("secret_length");

                System.out.println(String.format("User: %s has secret with length - %s", name, secretLength));
            }

            dropTableUsers(statement);
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.out.println("----------------");
    }

    @Test
    public void DateSaveTest() {
        try (
                Connection connection = createConnection();
                Statement statement = connection.createStatement()
        ) {
            statement.executeUpdate("drop table if exists jdbcdemo.Persons");
            statement.executeUpdate("create table if not EXISTS jdbcdemo.Persons (id int not null generated always as identity, name varchar(100) not null, birthday date not null, primary key (id));");

            PreparedStatement preparedStatement = connection.prepareStatement("insert into jdbcdemo.Persons (name, birthday) values (?, ?);");
            preparedStatement.setString(1, "Smith");
            long calendar = new Calendar.Builder().setDate(1955, Calendar.AUGUST, 15).build().getTimeInMillis();
            preparedStatement.setDate(2, new java.sql.Date(calendar), Calendar.getInstance());
            preparedStatement.executeUpdate();

            preparedStatement = connection.prepareStatement("select * from jdbcdemo.Persons;");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                String name = resultSet.getString("name");
                String birthday = resultSet.getString("birthday");
                System.out.println(String.format("Person - name: %s, birthday: %s", name, birthday));
            }

            preparedStatement.close();
            statement.executeUpdate("drop table if exists jdbcdemo.Persons");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.out.println("----------------");
    }

    @Test
    public void saveLargeObjectTest() throws IOException {
        String tmpSaveFilePath = "src/test/resources/save-thumbs-up-png.png";

        try (
                Connection connection = createConnection();
                Statement statement = connection.createStatement()
        ) {
            statement.executeUpdate("drop table if exists Photos");
            statement.executeUpdate("create table if not EXISTS Photos (id int not null generated always as identity, name text, img bytea, primary key (id));");

            File file = new File("src/test/resources/thumbs-up-png.png");
            FileInputStream fis = new FileInputStream(file);

            PreparedStatement preparedStatement = connection.prepareStatement("insert into Photos (name, img) values (?, ?);");
            preparedStatement.setString(1, file.getName());
            preparedStatement.setBinaryStream(2, fis, (int) file.length());
            preparedStatement.executeUpdate();

            fis.close();

            preparedStatement = connection.prepareStatement("SELECT img FROM Photos WHERE name = ?");
            preparedStatement.setString(1, file.getName());
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                byte[] imgBytes = resultSet.getBytes(1);
                try (FileOutputStream fos = new FileOutputStream(tmpSaveFilePath)) {
                    fos.write(imgBytes);
                }
            }
            resultSet.close();
            preparedStatement.close();

            statement.executeUpdate("drop table if exists Photos");

            try {
                Files.delete(Paths.get(tmpSaveFilePath));
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.out.println("----------------");
    }

    @Test
    public void prepareStatementTest() {
        try (
                Connection connection = createConnection();
                Statement statement = connection.createStatement()
        ) {
            createTableUsersAndPopulate(statement);

            int id = 2;
            PreparedStatement preparedStatement = connection.prepareStatement("select * from Users where id = ?;");
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            printAllUsersData(resultSet);

            preparedStatement.close();
            dropTableUsers(statement);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.out.println("----------------");
    }

    @Test
    public void statementInjectionTest() {
        try (
                Connection connection = createConnection();
                Statement statement = connection.createStatement()
        ) {
            createTableUsersAndPopulate(statement);

            String id = "2 or 1 = '1'";
            ResultSet resultSet = statement.executeQuery("select * from Users where id = " + id + ";");

            printAllUsersData(resultSet);

            dropTableUsers(statement);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.out.println("----------------");
    }

    @Test
    public void statementTest() {
        try (
                Connection connection = createConnection();
                Statement statement = connection.createStatement()
        ) {
            createTableUsersAndPopulate(statement);
            ResultSet resultSet = statement.executeQuery("select * from Users;");

            printAllUsersData(resultSet);

            dropTableUsers(statement);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.out.println("----------------");
    }

    @Test
    public void connectionTest() {
        try (Connection connection = createConnection()) {
            System.out.println("Connection successfully!");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.out.println("----------------");
    }

    private void createTableUsersAndPopulate(Statement statement) throws SQLException {
        createTableUsers(statement);
        populateTableUsers(statement);
    }

    private void createTableUsers(Statement statement) throws SQLException {
        statement.executeUpdate("drop table if exists Users");
        statement.executeUpdate("create table if not EXISTS Users (id int not null generated always as identity, name varchar(100) not null, secret varchar(100) not null, primary key (id));");
    }

    private void populateTableUsers(Statement statement) throws SQLException {
        statement.executeUpdate("insert into Users (name, secret) values ('Ann', 'a very secret secret');");
        statement.executeUpdate("insert into Users (name, secret) values ('Rick', 'not a very interesting secret ');");
        statement.executeUpdate("insert into Users (name, secret) values ('Nina', 'the secret that everyone knows ');");
    }

    private int getRowCount(Statement statement) throws SQLException {
        ResultSet resultSet = statement.executeQuery("SELECT COUNT(1) FROM Users;");
        return resultSet.next() ? resultSet.getInt(1) : 0;
    }

    private void dropTableUsers(Statement statement) throws SQLException {
        statement.executeUpdate("drop table if exists Users");
        statement.close();
    }

    private void printAllUsersData(ResultSet resultSet) throws SQLException {
        while (resultSet.next()) {
            printUserData(resultSet);
        }

        resultSet.close();
    }

    private void printUserData(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String name = resultSet.getString("name");
        String secret = resultSet.getString("secret");

        System.out.println(String.format("[Information about User with id: %s] %s has secret, it is %s", id, name, secret));
    }

    private Connection createConnection() throws SQLException {
        return DriverManager.getConnection(URL_CONNECTION, USERNAME, PASSWORD);
    }
}
