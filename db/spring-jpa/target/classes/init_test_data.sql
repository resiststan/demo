--tasks
INSERT INTO springjpa.tasks (id, description) VALUES (1, 'to be on the same wavelength with colleagues');
INSERT INTO springjpa.tasks (id, description) VALUES (2, 'have a good mood');
INSERT INTO springjpa.tasks (id, description) VALUES (3, 'make the world a better place');

--positions
INSERT INTO springjpa.positions (id, "name") VALUES (1, 'Director of Main Department');
INSERT INTO springjpa.positions (id, "name") VALUES (2, 'Assistant of Main Department');
INSERT INTO springjpa.positions (id, "name") VALUES (3, 'Resource allocator of Main Department');
INSERT INTO springjpa.positions (id, "name") VALUES (4, 'Security officer of Main Department');

INSERT INTO springjpa.positions (id, "name") VALUES (5, 'Director of North Department');
INSERT INTO springjpa.positions (id, "name") VALUES (6, 'Assistant of North Department');
INSERT INTO springjpa.positions (id, "name") VALUES (7, 'Resource allocator of North Department").build');
INSERT INTO springjpa.positions (id, "name") VALUES (8, 'Security officer of North Department');

INSERT INTO springjpa.positions (id, "name") VALUES (9, 'Director of South Department');
INSERT INTO springjpa.positions (id, "name") VALUES (10, 'Assistant of South Department").build');
INSERT INTO springjpa.positions (id, "name") VALUES (11, 'Resource allocator of South Department');
INSERT INTO springjpa.positions (id, "name") VALUES (12, 'Security officer of South Department');

INSERT INTO springjpa.positions (id, "name") VALUES (13, 'Director of West Department").build');
INSERT INTO springjpa.positions (id, "name") VALUES (14, 'Assistant of West Department');
INSERT INTO springjpa.positions (id, "name") VALUES (15, 'Resource allocator of West Department');
INSERT INTO springjpa.positions (id, "name") VALUES (16, 'Security officer of West Department").build');

INSERT INTO springjpa.positions (id, "name") VALUES (17, 'Director of East Department');
INSERT INTO springjpa.positions (id, "name") VALUES (18, 'Assistant of East Department');
INSERT INTO springjpa.positions (id, "name") VALUES (19, 'Resource allocator of East Department").build');
INSERT INTO springjpa.positions (id, "name") VALUES (20, 'Security officer of East Department');

--locations
INSERT INTO springjpa.locations (id, latitude_d1, latitude_d2, longitude_d1, longitude_d2) VALUES (1, 69, 191466, 33, 235617);
INSERT INTO springjpa.locations (id, latitude_d1, latitude_d2, longitude_d1, longitude_d2) VALUES (2, 43, 587769, 39, 736788);
INSERT INTO springjpa.locations (id, latitude_d1, latitude_d2, longitude_d1, longitude_d2) VALUES (3, 54, 722154, 20, 486398);
INSERT INTO springjpa.locations (id, latitude_d1, latitude_d2, longitude_d1, longitude_d2) VALUES (4, 43, 117312, 131, 912648);

--departments
INSERT INTO springjpa.departments (id, location_id, "name", description) VALUES (1, 2, 'Main Department', 'Head office');
INSERT INTO springjpa.departments (id, location_id, "name") VALUES (2, 1, 'North Department');
INSERT INTO springjpa.departments (id, location_id, "name") VALUES (3, 2, 'South Department');
INSERT INTO springjpa.departments (id, location_id, "name") VALUES (4, 3, 'West Department');
INSERT INTO springjpa.departments (id, location_id, "name") VALUES (5, 4, 'East Department');

--employees
INSERT INTO springjpa.employees (id, firstname, lastname, position_id, department_id) VALUES (1, 'Mari', 'Ross', 1, 1);
INSERT INTO springjpa.employees (id, firstname, lastname, position_id, department_id) VALUES (2, 'Nick', 'Moss', 2, 1);
INSERT INTO springjpa.employees (id, firstname, lastname, position_id, department_id) VALUES (3, 'Sho', 'Brown', 3, 1);
INSERT INTO springjpa.employees (id, firstname, lastname, position_id, department_id) VALUES (4, 'Geek', 'Break', 4, 1);

INSERT INTO springjpa.employees (id, firstname, lastname, position_id, department_id) VALUES (5, 'Roma', 'Rick', 5, 2);
INSERT INTO springjpa.employees (id, firstname, lastname, position_id, department_id) VALUES (6, 'Ann', 'Bell', 6, 2);
INSERT INTO springjpa.employees (id, firstname, lastname, position_id, department_id) VALUES (7, 'Fill', 'Car', 7, 2);
INSERT INTO springjpa.employees (id, firstname, lastname, position_id, department_id) VALUES (8, 'Lana', 'Don', 8, 2);

INSERT INTO springjpa.employees (id, firstname, lastname, position_id, department_id) VALUES (9, 'Kira', 'Hill', 9, 3);
INSERT INTO springjpa.employees (id, firstname, lastname, position_id, department_id) VALUES (10, 'Zak', 'Ver', 10, 3);
INSERT INTO springjpa.employees (id, firstname, lastname, position_id, department_id) VALUES (11, 'Lira', 'Age', 11, 3);
INSERT INTO springjpa.employees (id, firstname, lastname, position_id, department_id) VALUES (12, 'John', 'Tor', 12, 3);

INSERT INTO springjpa.employees (id, firstname, lastname, position_id, department_id) VALUES (13, 'Lisa', 'Mur', 13, 4);
INSERT INTO springjpa.employees (id, firstname, lastname, position_id, department_id) VALUES (14, 'Tom', 'Rod', 14, 4);
INSERT INTO springjpa.employees (id, firstname, lastname, position_id, department_id) VALUES (15, 'Lily', 'Pain', 15, 4);
INSERT INTO springjpa.employees (id, firstname, lastname, position_id, department_id) VALUES (16,'Lom', 'Save', 16, 4);

INSERT INTO springjpa.employees (id, firstname, lastname, position_id, department_id) VALUES (17, 'Ron', 'Guns', 17, 5);
INSERT INTO springjpa.employees (id, firstname, lastname, position_id, department_id) VALUES (18, 'Helga', 'Fi', 18, 5);
INSERT INTO springjpa.employees (id, firstname, lastname, position_id, department_id) VALUES (19, 'Mick', 'Ruk', 19, 5);
INSERT INTO springjpa.employees (id, firstname, lastname, position_id, department_id) VALUES (20, 'Zoi', 'Will', 20, 5);

--employees_tasks
INSERT INTO springjpa.employees_tasks (employee_id, task_id)
  SELECT e.id, t.id as employee_id FROM springjpa.employees as e JOIN springjpa.tasks as t ON 1=1;