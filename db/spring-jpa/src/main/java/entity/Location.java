package entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Entity
@Table(name = "locations")
public class Location {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "d1", column = @Column(name = "latitude_d1")),
            @AttributeOverride(name = "d2", column = @Column(name = "latitude_d2")),
    })
    private Angle latitude;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "d1", column = @Column(name = "longitude_d1")),
            @AttributeOverride(name = "d2", column = @Column(name = "longitude_d2")),
    })
    private Angle longitude;

    public Location(Angle latitude, Angle longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Getter
    @Setter
    @ToString
    @AllArgsConstructor
    @NoArgsConstructor
    @Embeddable
    public static class Angle {
        private int d1;
        private int d2;
    }
}
