package dao.impl;

import dao.BaseCrud;
import dao.BaseDaoFactory;
import entity.Task;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class TasksDaoTest {
    private static BaseCrud<Task, Long> tasksDao;
    private static List<Task> tasks;

    @BeforeClass
    public static void setUp() {
        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();

        BaseDaoFactory baseDaoFactory = new BaseDaoFactory(sessionFactory);
        tasksDao = baseDaoFactory.getDao(Task.class);

        tasks = Arrays.asList(
                Task.builder().description("to be on the same wavelength with colleagues").build(),
                Task.builder().description("have a good mood").build(),
                Task.builder().description("make the world a better place").build()
        );
    }

    @Test
    public void saveTest() {
        Task expectTask = tasks.get(0);
        Task actualTask = tasksDao.save(expectTask);

        Assert.assertNotNull(actualTask.getId());
        Assert.assertEquals(expectTask.getDescription(), actualTask.getDescription());
    }

    @Test
    public void findTest() {
        Task expectTask = tasks.get(1);

        tasksDao.save(expectTask);
        tasksDao.save(tasks.get(2));

        Long expectId = 2L;
        Task actualTask = tasksDao.find(expectId);

        Assert.assertEquals(expectId, actualTask.getId());
        Assert.assertEquals(expectTask.getDescription(), actualTask.getDescription());
    }

    @Test
    public void getAllTest() {
        List<Task> actualTasks = tasksDao.getAll();

        Assert.assertEquals(3, actualTasks.size());
    }

    @Test
    public void updateTest() {
        Task actualTask = tasksDao.find(2L);
        actualTask.setDescription("new task");

        tasksDao.update(actualTask);
        Task expectTask = tasksDao.find(2L);

        Assert.assertEquals(expectTask.getId(), actualTask.getId());
        Assert.assertEquals(expectTask.getDescription(), actualTask.getDescription());
    }

    @Test
    public void deleteTest() {
        Task deleteTask = tasksDao.find(3L);
        tasksDao.delete(deleteTask);
        List<Task> actualTasks = tasksDao.getAll();

        Task taskWithId3 = tasksDao.find(3L);

        Assert.assertEquals(2, actualTasks.size());
        Assert.assertNull(taskWithId3);
    }
}