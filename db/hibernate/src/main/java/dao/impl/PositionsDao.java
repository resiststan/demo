package dao.impl;

import dao.BaseDao;
import entity.Position;
import org.hibernate.SessionFactory;

public class PositionsDao extends BaseDao<Position, Long> {
    public PositionsDao(SessionFactory factory) {
        super(factory);
    }
}
