package dao.impl;

import dao.BaseDao;
import entity.Department;
import org.hibernate.SessionFactory;

public class DepartmentsDao extends BaseDao<Department, Long> {
    public DepartmentsDao(SessionFactory factory) {
        super(factory);
    }
}
