package net.resiststan.core.immutableobject;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotSame;

public class ImmutableObjectTest {
    @Test
    public void testImmutableObject() {
        long longField = 1;
        String string = "string";

        ImmutableObject immutableObjectExpected = new ImmutableObject(longField, string, new MutableObject());

        ImmutableObject immutableObject = new ImmutableObject(longField, string, new MutableObject());

        MutableObject mutableObject = immutableObject.getObjectField();
        mutableObject.setIntField(23);
        mutableObject.setLongField(42);
        mutableObject.setStringField("new string");

        assertNotSame(immutableObjectExpected, immutableObject);
        assertNotEquals(immutableObject.getObjectField(), mutableObject);
        assertEquals(immutableObjectExpected, immutableObject);
    }
}
