package net.resiststan.core.multithreading.waitnotifynotifyall;

public class Message {
    private final long id;
    private final String title;
    private final String text;

    public Message(long id, String title, String text) {
        this.id = id;
        this.title = title;
        this.text = text;
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return "Message{" +
                "title='" + title + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}
