package net.resiststan.core.multithreading.waitnotifynotifyall;

public class TextMessageHandler extends MessageHandler {
    public TextMessageHandler(Message message) {
        super(message);
    }

    @Override
    public void handle() {
        long id = message.getId();
        String text = message.getText();

        if (text == null) {
            System.out.println(String.format("%s: In message with id %s text is undefined", getTag(), id));
        } else {
            System.out.println(String.format("%s: In message with id %s text has length %s", getTag(), id, text.length()));
        }
    }


}
