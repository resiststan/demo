package net.resiststan.core.multithreading.waitnotifynotifyall;

public class MessageProvider extends Tag implements Runnable {
    private final Message message;

    public MessageProvider(Message message) {
        this.message = message;
    }

    @Override
    public void run() {
        System.out.println(getTag() + " started");
        try {
            Thread.sleep(1000);
            synchronized (message) {
//                System.out.println(TAG + " notify");
//                message.notify();

                System.out.println(getTag() + " notifyAll");
                message.notifyAll();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
