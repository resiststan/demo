package net.resiststan.core.multithreading.waitnotifynotifyall;

public class Main {
    public static void main(String[] args) {
        Message message = new Message(1, null, "some test");

        MessageHandler titleMessageHandler = new TitleMessageHandler(message);
        new Thread(titleMessageHandler, "waiter1").start();

        MessageHandler textMessageHandler = new TextMessageHandler(message);
        new Thread(textMessageHandler, "waiter2").start();

        MessageProvider messageProvider = new MessageProvider(message);
        new Thread(messageProvider, "notifier").start();

        System.out.println("All threads are running");
    }
}
