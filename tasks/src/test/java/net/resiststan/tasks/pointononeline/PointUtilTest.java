package net.resiststan.tasks.pointononeline;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;

import java.util.Arrays;
import java.util.Collection;

@RunWith(value = Parameterized.class)
public class PointUtilTest {
    @Parameter
    public Point[] points;
    @Parameter(value = 1)
    public int onOneLine;
    @Parameter(value = 2)
    public int expLinesCount;

    @Parameterized.Parameters(name = "{index}: {1} on one line exp - {2}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {
                        new Point[]{new Point(0, 0), new Point(1, 0), new Point(2, 0), new Point(3, 0),
                                new Point(3, 1), new Point(3, 2), new Point(3, 3), new Point(2, 3),
                                new Point(1, 3), new Point(0, 3), new Point(0, 2), new Point(0, 1),
                                new Point(4, 0), new Point(5, 0), new Point(5, 1), new Point(5, 2),
                                new Point(5, 3), new Point(5, 4)
                        },
                        4,
                        5
                },
                {
                        new Point[]{new Point(0, 0), new Point(1, 1), new Point(2, 2),
                                new Point(3, 3), new Point(4, 4)
                        },
                        3,
                        1
                }
        });
    }

    @Test
    public void findLinesTest() {
        PointUtil.Result lines = PointUtil.findLines(points, onOneLine);
        Assert.assertEquals(expLinesCount, lines.getSize());
        System.out.println(lines.toString());
    }
}
