package net.resiststan.tasks.stringsort1;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;

import java.util.Arrays;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class ImplTest {
    @Parameter(value = 0)
    public String[] arrAct;
    @Parameter(value = 1)
    public String[] arrExp;

    @Parameterized.Parameters(name = "{index}:act{0} exp{1}")
    public static Iterable<Object[]> dataForTest() {
        return Arrays.asList(new Object[][]{
                {new String[]{"аакаа", "ааажж", "ооооц", "абббб", "даадд", "яяяя"},
                        new String[]{"ооооц", "аакаа", "яяяя", "ааажж", "даадд", "абббб"}},

                {new String[]{"Кашалот", "Катафалк", "Шар", "Трактор", "Яблоко"},
                        new String[]{"Катафалк", "Кашалот", "Яблоко", "Трактор", "Шар"}},

                {new String[]{"Огонь", "Стена", "Паровоз", "Земля", "Мяч"},
                        new String[]{"Паровоз", "Земля", "Стена", "Огонь", "Мяч"}}
        });
    }

    @Test
    public void sortParamTest() {
        Impl.sort(arrAct);
        assertArrayEquals(arrExp, arrAct);
    }


}

