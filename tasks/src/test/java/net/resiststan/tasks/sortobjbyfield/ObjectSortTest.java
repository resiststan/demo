package net.resiststan.tasks.sortobjbyfield;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class ObjectSortTest {

    @Parameter(value = 0)
    public String field;
    @Parameter(value = 1)
    public List<TestClass1> expArr;
    @Parameter(value = 2)
    public List<TestClass1> actArr;

    @Parameters(name = "{index}: sort {0}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {

                        "name",
                        Arrays.asList(
                                new TestClass1("a", 50, 'f', 0.423),
                                new TestClass1("d", 10, '1', 3.34),
                                new TestClass1("y", 100, ' ', 1.2)
                        ),
                        createTestList()
                },
                {
                        "d",
                        Arrays.asList(
                                new TestClass1("a", 50, 'f', 0.423),
                                new TestClass1("y", 100, ' ', 1.2),
                                new TestClass1("d", 10, '1', 3.34)
                        ),
                        createTestList()
                },
                {
                        "c",
                        Arrays.asList(
                                new TestClass1("y", 100, ' ', 1.2),
                                new TestClass1("d", 10, '1', 3.34),
                                new TestClass1("a", 50, 'f', 0.423)
                        ),
                        createTestList()
                },
        });
    }

    @Test
    public void sortByFieldTest() {
        ObjectSort.sortByField(actArr, field);
        assertArrayEquals(expArr.toArray(), actArr.toArray());
    }

    private static List<TestClass1> createTestList() {
        return Arrays.asList(
                new TestClass1("y", 100, ' ', 1.2),
                new TestClass1("d", 10, '1', 3.34),
                new TestClass1("a", 50, 'f', 0.423)
        );
    }

    private static class TestClass1 {
        private String name;
        private Integer value;
        private char c;
        private double d;

        public TestClass1(String name, Integer value, char c, double d) {
            this.name = name;
            this.value = value;
            this.c = c;
            this.d = d;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getValue() {
            return value;
        }

        public void setValue(Integer value) {
            this.value = value;
        }

        public char getC() {
            return c;
        }

        public void setC(char c) {
            this.c = c;
        }

        public double getD() {
            return d;
        }

        public void setD(double d) {
            this.d = d;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            TestClass1 that = (TestClass1) o;

            if (c != that.c) return false;
            if (Double.compare(that.d, d) != 0) return false;
            if (!Objects.equals(name, that.name)) return false;
            return Objects.equals(value, that.value);
        }

        @Override
        public int hashCode() {
            return Objects.hash(name, value, c, d);
        }
    }
}
