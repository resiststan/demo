package net.resiststan.tasks.sortobjbyfield;

import java.lang.reflect.Field;
import java.util.*;

public class ObjectSort {
    public static List<String> getFieldList(Class cl) {
        List<String> fields = new ArrayList<>();

        while (cl != Object.class) {
            Field[] declaredFields = cl.getDeclaredFields();

            for (Field f : declaredFields) {
                fields.add(f.getName());
            }

            cl = cl.getSuperclass();
        }

        return fields;
    }

    public static <T> void sortByField(List<T> list, String fieldName) {
        if (list.size() > 0) {
            Collections.sort(list, new ReflectiveComparator(fieldName));
        }
    }

    private static class ReflectiveComparator implements Comparator<Object> {
        private String fieldName;

        public ReflectiveComparator(String fieldName) {
            this.fieldName = fieldName;
        }

        @Override
        public int compare(Object o1, Object o2) {
            try {
                Field field = o1.getClass().getDeclaredField(fieldName);
                if (!Comparable.class.isAssignableFrom(field.getType())) {
                    switch (field.getType().getName()) {
                        case "char":
                        case "int":
                        case "long":
                        case "byte":
                        case "boolean":
                        case "short":
                        case "float":
                        case "double":
                            break;
                        default:
                            throw new IllegalStateException("Field not Comparable: " + field + " (field type " + field.getType() + ")");
                    }
                }
                field.setAccessible(true);
                Comparable o1FieldValue = (Comparable) field.get(o1);
                Comparable o2FieldValue = (Comparable) field.get(o2);
                return o1FieldValue.compareTo(o2FieldValue);
            } catch (NoSuchFieldException e) {
                throw new IllegalStateException("Field doesn't exist", e);
            } catch (IllegalAccessException e) {
                throw new IllegalStateException("Field inaccessible", e);
            }
        }
    }
}
