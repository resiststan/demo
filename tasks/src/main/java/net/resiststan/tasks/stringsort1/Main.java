package net.resiststan.tasks.stringsort1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    private static Scanner in;
    private static int count;
    private static String[] arr;

    public static void main(String[] args) {
        in = new Scanner(System.in);

        System.out.println("введите количество слов:");
        readCount();

        System.out.println("введите слова через пробел:");
        readWords();

        Impl.sort(arr);

        System.out.println("отсортированный массив:");
        print(arr);
    }

    private static void print(String[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }

        System.out.println();
    }

    private static void readWords() {
        for(int i = 0; i < count; i++) {
            arr[i] = in.next();
        }
    }

    private static void readCount() {
        boolean validInput;

        do {
            try {
                count = in.nextInt();
                arr = new String[count];
                validInput = true;
            } catch (InputMismatchException e) {
                validInput = false;
            }
        } while (!validInput);
    }
}

