package net.resiststan.tasks.calculator;

interface ICalc {
    public Double Add(Double a, Double b);

    public Double Divide(Double a, Double b);

    public Double Multiply(Double a, Double b);
}
