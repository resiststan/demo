package net.resiststan.tasks.calculator;

public class SCalculator implements ICalc {
    public Double Add(Double a, Double b) {
        return a + b;
    }

    public Double Divide(Double a, Double b) {
        return a / b;
    }

    public Double Multiply(Double a, Double b) {
        return a * b;
    }
}
