package net.resiststan.tasks.pointononeline;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class PointUtil {
    private static List<List<Point>> pL;

    /**
     * Find all lines that contains minimum c point
     *
     * @param a array of Point @link{Point}
     * @param c minimum point on one line
     * @return
     */
    public static Result findLines(Point[] a, final int c) {
        pL = new ArrayList<List<Point>>();
        Arrays.sort(Arrays.copyOf(a, a.length));

        if (c > 2) {
            for (int p0 = 0; p0 < a.length - (c - 1); p0++) {
                //определяем нулевую току
                int x0 = a[p0].X;
                int y0 = a[p0].Y;

                for (int p1 = p0 + 1; p1 < a.length - (c - 2); p1++) {
                    List<Point> tmppl = new ArrayList<Point>();
                    tmppl.add(a[p0]);
                    tmppl.add(a[p1]);

                    //проверка вхождения в существующие прямые
                    if (checkContain(tmppl)) {
                        continue;
                    }

                    for (int p2 = p1 + 1; p2 < a.length; p2++) {
                        //на одной ли линни эти точки
                        if (onOneLine(a[p0], a[p1], a[p2])) {
                            tmppl.add(a[p2]);
                        }
                    }

                    if (tmppl.size() >= c) {
                        pL.add(tmppl);
                    }
                }
            }
        }

        return new Result(pL);
    }

    private static boolean onOneLine(Point p1, Point p2, Point p3) {
        return (p1.X - p3.X) * (p2.Y - p3.Y) - (p2.X - p3.X) * (p1.Y - p3.Y) == 0;
    }

    private static boolean checkContain(List<Point> list) {
        for (int i = 0; i < pL.size(); i++) {
            if (pL.get(i).contains(list.get(0)) && pL.get(i).contains(list.get(1))) {
                return true;
            }
        }

        return false;
    }

    static class Result {
        final List<List<Point>> pl;

        private Result(List<List<Point>> pl) {
            this.pl = pl;
        }

        public int getSize() {
            return pl.size();
        }

        public Iterator<List<Point>> getLines() {
            return pl.iterator();
        }

        @Override
        public String toString() {
            var sb = new StringBuilder();
            pl.forEach(l -> {
                sb.append(l).append("\n");
            });
            return sb.toString();
        }
    }
}
