package net.resiststan.patterns.structural.adapter;

import net.resiststan.patterns.structural.adapter.legacythirdparty.LegacyObject;
import net.resiststan.patterns.structural.adapter.ourproj.LegacyObjectAdapter;
import net.resiststan.patterns.structural.adapter.ourproj.NewObject;
/*
Шаблон: Адаптер (Adapter)

Цель:
- Преобразование интерфейса одного класса в интерфейс того класса, который необходим клиенту.

Для чего используется:
- Для обеспечения совместной работы классов, интерфейсы которых не совместимы.

Пример использования:
- интерфейс класса, который мы хотим использовать не соответствует нашим потребностям;
- необходим класс, который должен взаимодействовать с классами, которые ему неизвестны
  или не связаны с ним;
- необходимо использовать несколько существующих подклассов, но нецелесообразно использовать
  методы этих классов создавая их новые подклассы.
*/
public class DemoAdapter {
    public static void main(String[] args) {
        NewObject obj = new LegacyObjectAdapter(new LegacyObject());

        obj.create();
        obj.read();
        obj.update();
        obj.delete();
    }
}
