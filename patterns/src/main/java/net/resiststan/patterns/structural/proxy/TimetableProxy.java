package net.resiststan.patterns.structural.proxy;

public class TimetableProxy implements Timetable {
    private Timetable timetable;

    @Override
    public void getData() {
        if (timetable == null) {
            timetable = new TrafficTimetable();
        }

        timetable.getData();
    }
}
