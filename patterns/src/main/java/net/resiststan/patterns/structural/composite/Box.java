package net.resiststan.patterns.structural.composite;

import java.util.ArrayList;
import java.util.List;

public class Box implements Composite, Leaf {
    private List<Component> children = new ArrayList<>();

    @Override
    public List<Component> getChildren() {
        return children;
    }

    @Override
    public boolean add(Component component) {
        return children.add(component);
    }

    @Override
    public boolean remove(Component component) {
        return children.remove(component);
    }

    @Override
    public String toString() {
        return "Box" + this.hashCode() + "{" +
                "children=" + children +
                '}';
    }
}
