package net.resiststan.patterns.structural.proxy;

public interface Timetable {
    void getData();
}
