package net.resiststan.patterns.structural.bridge.vehiceltype;

import net.resiststan.patterns.structural.bridge.Manufacture;
import net.resiststan.patterns.structural.bridge.Vehicle;

public class Track extends Vehicle {
    public Track(Manufacture manufacture) {
        super(manufacture);
    }
}
