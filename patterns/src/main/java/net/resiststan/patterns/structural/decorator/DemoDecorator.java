package net.resiststan.patterns.structural.decorator;

/*
Шаблон: Декоратор(Decorator)

* Цель:
- Динамическое добавление новых обязанностей объекту.

Для чего используется:
- Используется в качестве альтернативы порождению подклассов для расширения функциональности.

Пример использования:
- динамическое и понятное клиентам добавления обязанностей объектам;
- реализация обязанностей, которые могут быть сняты с объекта;
- расширение класса путём порождения подклассов невозможно по каким-либо причинам.
*/
public class DemoDecorator {
    public static void main(String[] args) {
        Developer developer = new SeniorDeveloper(new MiddleDeveloper(new JuniorDeveloper()));

        developer.makeJob();
    }
}
