package net.resiststan.patterns.structural.adapter.ourproj;

public class NewObjectImpl implements NewObject {
    @Override
    public void create() {
        System.out.println("Create object...");
    }

    @Override
    public void read() {
        System.out.println("Read object...");
    }

    @Override
    public void update() {
        System.out.println("Update object...");
    }

    @Override
    public void delete() {
        System.out.println("Delete object...");
    }
}
