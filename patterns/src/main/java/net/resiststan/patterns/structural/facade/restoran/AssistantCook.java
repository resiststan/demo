package net.resiststan.patterns.structural.facade.restoran;

import java.util.Arrays;
import java.util.List;

public class AssistantCook implements Cook {
    @Override
    public Recipe makeRecipe() {
        return new Recipe();
    }

    @Override
    public void giveAssignmentTo(Cook assistantCook) {

    }

    @Override
    public List<Product> prepareProduct() {
        return Arrays.asList(new Product(), new Product());
    }

    @Override
    public Lunch make(Order order, List<Product> preparedProducts, Recipe recipe) {
        return new Lunch();
    }
}
