package net.resiststan.patterns.structural.adapter.ourproj;

import net.resiststan.patterns.structural.adapter.legacythirdparty.LegacyObject;

public class LegacyObjectAdapter implements NewObject {

    private final LegacyObject legacyObject;

    public LegacyObjectAdapter(LegacyObject legacyObject) {
        this.legacyObject = legacyObject;
    }

    @Override
    public void create() {
        legacyObject.createNew();
    }

    @Override
    public void read() {
        legacyObject.declaim();
    }

    @Override
    public void update() {
        legacyObject.edit();
    }

    @Override
    public void delete() {
        legacyObject.remove();
    }
}
