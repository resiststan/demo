package net.resiststan.patterns.structural.adapter.legacythirdparty;

public class LegacyObject {
    public void createNew() {
        System.out.println("Create" + action());
    }

    public void declaim() {
        System.out.println("Declaim" + action());
    }

    public void edit() {
        System.out.println("Edit" + action());
    }

    public void remove() {
        System.out.println("Remove" + action());
    }

    private String action() {
        return " " + this.getClass().getSimpleName() + " object...";
    }
}
