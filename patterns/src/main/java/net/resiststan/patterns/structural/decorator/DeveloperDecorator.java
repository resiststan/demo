package net.resiststan.patterns.structural.decorator;

public abstract class DeveloperDecorator implements Developer {
    protected Developer developer;

    DeveloperDecorator(Developer developer) {
        this.developer = developer;
    }
}
