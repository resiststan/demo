package net.resiststan.patterns.structural.decorator;

public class SeniorDeveloper extends DeveloperDecorator {
    SeniorDeveloper(Developer developer) {
        super(developer);
    }

    @Override
    public void makeJob() {
        makeReview();
        developer.makeJob();
    }

    private void makeReview() {
        System.out.println("Make review code.");
    }
}
