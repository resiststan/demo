package net.resiststan.patterns.structural.flyweight;

public class FableWriter implements Writer {
    @Override
    public void doWork() {
        System.out.println("Write fable.");
    }
}
