package net.resiststan.patterns.structural.bridge.vehiceltype;

import net.resiststan.patterns.structural.bridge.Manufacture;
import net.resiststan.patterns.structural.bridge.Vehicle;

public class Motorbike extends Vehicle {
    public Motorbike(Manufacture manufacture) {
        super(manufacture);
    }
}
