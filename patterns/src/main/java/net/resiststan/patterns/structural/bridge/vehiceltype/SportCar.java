package net.resiststan.patterns.structural.bridge.vehiceltype;

import net.resiststan.patterns.structural.bridge.Manufacture;
import net.resiststan.patterns.structural.bridge.Vehicle;

public class SportCar extends Vehicle {
    public SportCar(Manufacture manufacture) {
        super(manufacture);
    }
}
