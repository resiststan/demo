package net.resiststan.patterns.structural.bridge;

import net.resiststan.patterns.structural.bridge.manufacture.Audi;
import net.resiststan.patterns.structural.bridge.manufacture.BMW;
import net.resiststan.patterns.structural.bridge.manufacture.Icarus;
import net.resiststan.patterns.structural.bridge.vehiceltype.Bus;
import net.resiststan.patterns.structural.bridge.vehiceltype.Motorbike;
import net.resiststan.patterns.structural.bridge.vehiceltype.SportCar;
import net.resiststan.patterns.structural.bridge.vehiceltype.Track;

/*
Шаблон: Мост (Bridge)

Цель:
- Отделить абстракцию от её реализации таким образом, чтобы мы могли изменять
независимо друг от друга и то и другое.

Для чего используется:
- Для получения преимуществ наследования без потери гибкости.

Пример использования:
- предотвращения жёсткого привязки абстракции к реализации (например, реализацию необходимо выбрать
  во время выполнения программы);
- в будущем мы хотим расширять с помощью подклассов и реализацию и абстракцию;
- если изменения в реализации не должны отражаться на клиентах абстракции;
- для разделения одной реализации между несколькими оъектами и не показывать это клиенту.
*/
public class DemoBridge {
    public static void main(String[] args) {
        Vehicle[] vehicles = {
                new SportCar(new Audi()),
                new Bus(new Icarus()),
                new Motorbike(new BMW()),
                new Motorbike(new Audi()),
                new Track(new Audi()),
        };

        for (Vehicle v : vehicles)
            v.drive();
    }
}
