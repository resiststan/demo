package net.resiststan.patterns.structural.facade.restoran;

public class Waiter {
    public Order getOrder(Order order) {
        return order;
    }

    public void takeOrder(Cook cook) {

    }
}
