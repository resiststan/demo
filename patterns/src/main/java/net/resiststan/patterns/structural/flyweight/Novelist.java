package net.resiststan.patterns.structural.flyweight;

public class Novelist implements Writer {
    @Override
    public void doWork() {
        System.out.println("Write novel.");
    }
}
