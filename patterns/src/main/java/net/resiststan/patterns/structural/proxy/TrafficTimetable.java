package net.resiststan.patterns.structural.proxy;

public class TrafficTimetable implements Timetable {
    public TrafficTimetable() {
        System.out.println("Generate data ...");
    }

    @Override
    public void getData() {
        System.out.println("Get data ...");
    }

}
