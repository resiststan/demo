package net.resiststan.patterns.behavioral.interpreter;

import java.util.Map;

public class NumberInterpreter {
    private static Map<String, Expression<String, String>> map;
    private static TypeExpression typeExpression;

    static {
        Expression<Integer, String> numberExpression = new NumberExpression();
        map = Map.of(
                "B", new IntToBinaryExpression(numberExpression),
                "H", new IntToHexExpression(numberExpression)
        );

        typeExpression = new TypeExpression();
    }

    public static String get(String str) {
        return map.get(typeExpression.interpret(str)).interpret(str);
    }
}
