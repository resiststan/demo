package net.resiststan.patterns.behavioral.visitor;

public class Cooking implements Work {
    @Override
    public void doWork(Visitor visitor) {
        visitor.cook(this);
    }
}
