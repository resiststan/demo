package net.resiststan.patterns.behavioral.iterator;

public interface Iterator<T> {
    boolean hasNext();

    T next();
}
