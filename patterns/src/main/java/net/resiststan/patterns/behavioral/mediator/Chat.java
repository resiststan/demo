package net.resiststan.patterns.behavioral.mediator;

public interface Chat {
    void addUser(User user);
    void sendMessage(User user, String message);
}
