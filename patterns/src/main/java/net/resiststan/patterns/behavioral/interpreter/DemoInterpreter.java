package net.resiststan.patterns.behavioral.interpreter;

/*
Шаблон: Интерпретатор (Interpreter)

Цель:
- Определение представления грамматики объекта

Для чего используется:
- Используется для определения представления грамматики заданного языка и интерпретации его предложений

Пример использования:
- упрощение иерархии классов с помощью интерпретирования.
*/
public class DemoInterpreter {
    public static void main(String[] args) {
        System.out.println(NumberInterpreter.get("15b"));
        System.out.println(NumberInterpreter.get("15h"));
        System.out.println(NumberInterpreter.get("28b"));
        System.out.println(NumberInterpreter.get("28h"));
    }
}
