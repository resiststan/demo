package net.resiststan.patterns.behavioral.strategy;

public class Professional implements Painter {
    @Override
    public void paint() {
        System.out.println("Price: a little expensive than wanted.");
        System.out.println("Quality: perfect.");
        System.out.println("Speed: normal.");
    }
}
