package net.resiststan.patterns.behavioral.state;

public class DefaultState implements State {
    private final State state = new UpperCaseState();

    @Override
    public State nextState() {
        return state;
    }

    @Override
    public void doAction(Context context) {

    }
}
