package net.resiststan.patterns.behavioral.visitor;

public class Cleaning implements Work {
    @Override
    public void doWork(Visitor visitor) {
        visitor.clean(this);
    }
}
