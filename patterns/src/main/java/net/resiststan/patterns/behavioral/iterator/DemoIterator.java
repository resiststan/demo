package net.resiststan.patterns.behavioral.iterator;

/*
Шаблон: Итератор (Iterator)

Цель:
- Получение последовательного доступа ко всем элементам составного объекта.

Для чего используется:
- Для получения последовательного доступа ко всем элементам составного объекта,
скрывая его внутреннее представление.

Пример использования:
- различные виды обхода составного объекта;
- упрощённый доступ к составному объекту.
*/
public class DemoIterator {
    public static void main(String[] args) {
        SomeCollection<String> sc = new SomeCollection<>(new String[]{"a", "b", "c"});
        sc.add("d");
        sc.add("e");

        Iterator<String> iterator = sc.iterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
