package net.resiststan.patterns.behavioral.interpreter;

public class TypeExpression implements Expression<String, String> {
    @Override
    public String interpret(String context) {
        return context.substring(context.length() - 1).toUpperCase();
    }
}
