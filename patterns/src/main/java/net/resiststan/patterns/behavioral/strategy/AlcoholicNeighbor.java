package net.resiststan.patterns.behavioral.strategy;

public class AlcoholicNeighbor implements Painter {
    @Override
    public void paint() {
        System.out.println("Price: one bottle of vodka.");
        System.out.println("Quality: so so...");
        System.out.println("Speed: fast.");
    }
}
