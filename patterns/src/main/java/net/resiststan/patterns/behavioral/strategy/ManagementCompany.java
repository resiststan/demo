package net.resiststan.patterns.behavioral.strategy;

public class ManagementCompany {
    private Painter painter;

    public ManagementCompany(Painter painter) {
        this.painter = painter;
    }

    public void paintBuilding() {
        painter.paint();
    }
}
