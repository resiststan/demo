package net.resiststan.patterns.behavioral.observer;

import java.util.ArrayList;
import java.util.List;

public class EventPublisher implements Publisher {
    private final List<Subscriber> subscribers = new ArrayList<>();

    public void newEvent(String event) {
        notifySubscribers(event);
    }

    @Override
    public void addSubscriber(Subscriber subscriber) {
        System.out.println("add subscriber " + subscriber);
        subscribers.add(subscriber);
    }

    @Override
    public void removeSubscriber(Subscriber subscriber) {
        System.out.println("remove subscriber " + subscriber);
        subscribers.remove(subscriber);
    }

    @Override
    public void notifySubscribers(String event) {
        for (Subscriber s : subscribers) {
            s.update(event);
        }
    }
}
