package net.resiststan.patterns.behavioral.templatemethod;

public class LionLife extends AnimalLifeTemplate {
    @Override
    void eat() {
        System.out.println("Go to meadow and try to catch zebra.");
    }

    @Override
    void routine() {
        System.out.println("Gain strength.");
    }
}
