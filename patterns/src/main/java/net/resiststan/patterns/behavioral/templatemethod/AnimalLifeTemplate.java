package net.resiststan.patterns.behavioral.templatemethod;

public abstract class AnimalLifeTemplate {
    abstract void eat();
    abstract void routine();

    public void wakeUp() {
        System.out.println("Wake up.");
    }

    public void sleep() {
        System.out.println("Go to sleep.");
    }

    public void playLife() {
        System.out.println(this.getClass().getSimpleName() + ":");

        wakeUp();
        eat();
        routine();
        sleep();

        System.out.println();
    }
}
