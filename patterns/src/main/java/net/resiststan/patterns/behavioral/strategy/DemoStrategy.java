package net.resiststan.patterns.behavioral.strategy;

/*
Шаблон: Стратегия (Strategy)

Цель:
- Взаимозаменяемость семейства классов

Для чего используется:
- Опредление семейства классов, инкапсулирование каждого из них и организация их взаимозаменяемости.

Пример использования:
- есть несколько родственных классов, которые отличаются поведением;
- необходимо иметь несколько вариантов поведения;
- в классе есть данные, о которых не должен знать клиент;
- с помощью условных операторов в классе определено большое количество возможных поведений.
*/
public class DemoStrategy {
    public static void main(String[] args) {
        ManagementCompany managementCompany = new ManagementCompany(new Professional());

        managementCompany.paintBuilding();
    }
}
