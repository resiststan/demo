package net.resiststan.patterns.behavioral.interpreter;

public interface Expression<T, C> {
    T interpret(C context);
}
