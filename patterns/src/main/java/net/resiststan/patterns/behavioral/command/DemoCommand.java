package net.resiststan.patterns.behavioral.command;

/*
Шаблон: Команда (Command)

Цель:
- Инкапсулирование запроса в объект.

Для чего используется:
- Чтобы задать параметры клиентов для обработки определённых запросов, создание
очереди из этих запросов или их контроля и поддержки отмены операций.

Пример использования:
- параметризация объектов выполняемым дейтсвием;
- определять запрос, ставить его в очередь или выполнять его в разное время
*/
public class DemoCommand {
    public static void main(String[] args) {
        Vehicle car = new Car();

        Outbid outbid = new Outbid(new Buy(car), new Inspection(car), new Repair(car), new Sell(car));

        outbid.inspectCar();
        outbid.buyCar();
        outbid.repairCar();
        outbid.sellCar();
    }
}
