package net.resiststan.patterns.behavioral.iterator;

public interface Collection<T> {
    Iterator<T> iterator();
}
