package net.resiststan.patterns.behavioral.state;

public class UpperCaseState implements State {
    private final State state = new LowerCaseState();


    @Override
    public State nextState() {
        return state;
    }

    @Override
    public void doAction(Context context) {
        Field field = (Field)context;
        field.setText(field.getText().toUpperCase());
    }
}
