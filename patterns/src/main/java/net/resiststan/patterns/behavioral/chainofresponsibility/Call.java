package net.resiststan.patterns.behavioral.chainofresponsibility;

public class Call {
    private ProblemLevel problemLevel;

    public Call(ProblemLevel problemLevel) {
        this.problemLevel = problemLevel;
    }

    public ProblemLevel getProblemLevel() {
        return problemLevel;
    }
}
