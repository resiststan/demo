package net.resiststan.patterns.behavioral.state;

/*
Шаблон: Состояние (State)

Цель:
- Управление поведением объекта в зависимости от состояния.

Для чего используется:
- Позволяет нам управлять поведением объекта в зависимости от внутреннего состояние объекта.

Пример использования:
- поведение объекта зависит от его состояние и изменяется во время выполнения;
- когда встречается большое количество условных операторов, когда выбор ветви
  зависит от состояния объекта.
*/
public class DemoState {
    public static void main(String[] args) {
        Field field = new Field("Hello World!");
        System.out.println(field);

        field.changeState();
        System.out.println(field);

        field.changeState();
        System.out.println(field);

        field.setState(new UpperCaseState());
        System.out.println(field);
    }
}
