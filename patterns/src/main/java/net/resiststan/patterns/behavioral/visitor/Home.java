package net.resiststan.patterns.behavioral.visitor;

public class Home implements Work {
    private Work[] works;

    public Home(Work[] works) {
        this.works = works;
    }

    @Override
    public void doWork(Visitor visitor) {
        for (Work w : works) {
            w.doWork(visitor);
        }
    }
}
