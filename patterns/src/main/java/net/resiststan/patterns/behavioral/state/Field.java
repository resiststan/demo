package net.resiststan.patterns.behavioral.state;

public class Field implements Context {
    private State state;
    private String text;

    public Field(String text) {
        this.text = text;
        state = new DefaultState();
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public State getState() {
        return state;
    }

    @Override
    public void setState(State state) {
        this.state = state;
        this.state.doAction(this);
    }

    @Override
    public void changeState() {
        if (state.nextState() != null) {
            state = state.nextState();
            state.doAction(this);
        }
    }

    @Override
    public String toString() {
        return "Field{" +
                "state=" + state +
                ", text='" + text + '\'' +
                '}';
    }
}
