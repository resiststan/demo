package net.resiststan.patterns.creational.builder;

public class Pizza {
    private boolean dough;
    private String name;
    private boolean cheese;
    private boolean meat;
    private double pepper;
    private double salt;
    private int salami;
    private int tomatoes;
    //...and other 100500 fields

    private Pizza(PizzaBuilder builder) {
        this.dough = builder.dough;
        this.name = builder.name;
        this.cheese = builder.cheese;
        this.meat = builder.meat;
        this.pepper = builder.pepper;
        this.salt = builder.salt;
        this.salami = builder.salami;
        this.tomatoes = builder.tomatoes;
    }

    public boolean getDough() {
        return dough;
    }

    public String getName() {
        return name;
    }

    public boolean getCheese() {
        return cheese;
    }

    public boolean getMeat() {
        return meat;
    }

    public double getPepper() {
        return pepper;
    }

    public double getSalt() {
        return salt;
    }

    public int getSalami() {
        return salami;
    }

    public double getTomatoes() {
        return tomatoes;
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "dough=" + dough +
                ", name='" + name + "\'" +
                ", cheese=" + cheese +
                ", meat=" + meat +
                ", pepper=" + pepper +
                ", salt=" + salt +
                ", salami=" + salami +
                ", tomatoes=" + tomatoes +
                '}';
    }

    public static PizzaBuilder newPizza(String name) {
        return new PizzaBuilder(name);
    }

    public static class PizzaBuilder {
        private boolean dough = true;
        private String name;
        private boolean cheese;
        private boolean meat;
        private int pepper;
        private int salt;
        private int salami;
        private int tomatoes;
        //...and other 100500 fields


        public PizzaBuilder(String name) {
            this.name = name;
        }

        public PizzaBuilder setDough(boolean dough) {
            this.dough = dough;
            return this;
        }

        public PizzaBuilder setCheese(boolean cheese) {
            this.cheese = cheese;
            return this;
        }

        public PizzaBuilder setMeat(boolean meat) {
            this.meat = meat;
            return this;
        }

        public PizzaBuilder setPepper(int pepper) {
            this.pepper = pepper;
            return this;
        }

        public PizzaBuilder setSalt(int salt) {
            this.salt = salt;
            return this;
        }

        public PizzaBuilder setSalami(int salami) {
            this.salami = salami;
            return this;
        }

        public PizzaBuilder setTomatoes(int tomatoes) {
            this.tomatoes = tomatoes;
            return this;
        }

        public Pizza build() {
            return new Pizza(this);
        }
    }
}
