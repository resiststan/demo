package net.resiststan.patterns.creational.abstractfactory;

public interface FurnitureFactory {
    static FurnitureFactory getFactory(FurnitureFactory furnitureFactory) {
        return furnitureFactory;
    }

    Chair createChair();

    Table createTable();

    Sofa createSofa();
}
