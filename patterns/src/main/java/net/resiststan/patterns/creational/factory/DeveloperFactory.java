package net.resiststan.patterns.creational.factory;

public class DeveloperFactory {
    public static Developer createDeveloper(Class<? extends Developer> developerClass) throws Exception {
        if (developerClass.equals(JavaDeveloper.class)) {
            return new JavaDeveloper();
        } else if (developerClass.equals(CppDeveloper.class)) {
            return new CppDeveloper();
        } else if (developerClass.equals(PythonDeveloper.class)) {
            return new PythonDeveloper();
        } else if (developerClass.equals(JSDeveloper.class)) {
            return new JSDeveloper();
        }

        throw new Exception("Dont exist case for " + developerClass);
    }
}
