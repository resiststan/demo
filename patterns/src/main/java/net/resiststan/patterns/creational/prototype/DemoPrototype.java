package net.resiststan.patterns.creational.prototype;

/*
Шаблон: Прототип (Prototype)

Цель:
- Определить вид создаваемых объектов с помощью экземпляра - прототипа и
создавать новые объекты, копируя этот прототип.

Для чего используется:
- Для создания копий заданного объекта.

Пример использования:
- классы, экземпляры которых необходимо создать определяются во время выполнения программы;
- для избежания построения иерархии классов, фабрик или параллельных иерархий классов;
- экземпляры класса могут находиться в одном из немногих возможных состояний.
*/
public class DemoPrototype {
    public static void main(String[] args) {
        Shape circle = new Circle();
        Shape cloneCircle = circle.copy();

        System.out.println("identical - " + circle.equals(cloneCircle));
        System.out.println("same obj - " + (circle == cloneCircle));

    }
}
