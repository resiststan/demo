package net.resiststan.patterns.creational.prototype;

import java.util.Objects;

public class Circle extends Shape {
    private int radius;

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public Circle() {
    }

    private Circle(Circle src) {
        super(src);
        if (src != null) {
            this.radius = src.radius;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Circle)) return false;

        Circle circle = (Circle) o;

        return Objects.equals(this.radius, circle.radius)
                && Objects.equals(this.getColor(), circle.getColor())
                && Objects.equals(this.getX(), circle.getX())
                && Objects.equals(this.getY(), circle.getY());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getX(), getY(), getColor(), radius);
    }

    @Override
    public Shape copy() {
        return new Circle(this);
    }
}
