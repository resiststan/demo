package net.resiststan.patterns.creational.abstractfactory.modern;

import net.resiststan.patterns.creational.abstractfactory.Chair;
import net.resiststan.patterns.creational.abstractfactory.FurnitureFactory;
import net.resiststan.patterns.creational.abstractfactory.Sofa;
import net.resiststan.patterns.creational.abstractfactory.Table;

public class ModernFactory implements FurnitureFactory {
    @Override
    public Chair createChair() {
        return new ModernChair();
    }

    @Override
    public Table createTable() {
        return new ModernTable();
    }

    @Override
    public Sofa createSofa() {
        return new ModernSofa();
    }
}
