package net.resiststan.algorithms.compression;

import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

public class SIHuffmanCodeTest {
    private final static String sourceString = "Hello World!";
    private final static String encodingString = "0110001101011111000101110111100001101";

    @Test
    public void encodingTest() {
        SIHuffmanCode.EncodingString encoding = new SIHuffmanCode.EncodingString(sourceString);
        assertEquals(encodingString, encoding.getEncodeString());
    }

    @Test
    public void decodingTest() {
        int mapSize = 9;
        int codeSize = 37;
        Map<String, Character> map = Map.of("000", 'd',
                "001", 'e',
                "010", 'W',
                "0110", 'H',
                "0111", 'r',
                "10", 'l',
                "1100", ' ',
                "1101", '!',
                "111", 'o'
        );

        SIHuffmanCode.DecodingString decoding = new SIHuffmanCode.DecodingString(mapSize, codeSize, map, encodingString);
        assertEquals(sourceString, decoding.getSourceString());

    }
}