package net.resiststan.algorithms.search.simple;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SearchTest {
    @Test
    public void binSearchTest() {
        int[] a = {-10, 1, 2, 3, 4, 5, 7, 8, 12, 43};
        Bin binSearch = new Bin();
        assertEquals(6, Search.find(7, a, binSearch));
        assertEquals(-1, Search.find(20, a, binSearch));
        assertEquals(-1, Search.find(-1, a, binSearch));
        assertEquals(0, Search.find(-10, a, binSearch));

        a = new int[]{1, 2, 3, 4, 5, 6, 7};
        assertEquals(6, Search.find(7, a, binSearch));
        assertEquals(-1, Search.find(20, a, binSearch));
        assertEquals(-1, Search.find(-1, a, binSearch));
        assertEquals(0, Search.find(1, a, binSearch));
    }

    @Test
    public void linearSearchTest() {
        int[] a = {-10, 1, 2, 3, 4, 5, 7, 8, 12, 43};
        Linear linearSearch = new Linear();
        assertEquals(6, Search.find(7, a, linearSearch));
        assertEquals(-1, Search.find(20, a, linearSearch));
        assertEquals(-1, Search.find(-1, a, linearSearch));
        assertEquals(0, Search.find(-10, a, linearSearch));

        a = new int[]{1, 2, 3, 4, 5, 6, 7};
        assertEquals(6, Search.find(7, a, linearSearch));
        assertEquals(-1, Search.find(20, a, linearSearch));
        assertEquals(-1, Search.find(-1, a, linearSearch));
        assertEquals(0, Search.find(1, a, linearSearch));
    }
}
