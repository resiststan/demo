package net.resiststan.algorithms.compression;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class SIHuffmanCode {


    public static class DecodingString {
        private int charMapSize;
        private int codeSize;
        private static String sourceString;
        private String encodeString;

        public DecodingString(int charMapSize, int codeSize, Map<String, Character> codeMap, String encodeString) {
            this.charMapSize = charMapSize;
            this.codeSize = codeSize;
            this.encodeString = encodeString;

            StringBuilder result = new StringBuilder();

            int start = 0;
            int end = 1;
            do {
                String subStr = encodeString.substring(start, end);
                if (codeMap.containsKey(subStr)) {
                    result.append(codeMap.get(subStr));

                    start = end;
                }

            } while (end++ != codeSize);
            sourceString = result.toString();
        }

        public String getSourceString() {
            return sourceString;
        }

        @Override
        public String toString() {
            return sourceString + "\n" +
                    charMapSize + " " + codeSize + "\n" +
                    encodeString;
        }
    }

    public static class EncodingString {
        private String sourceString;
        private int charMapSize;
        private int codeSize;
        private StringBuffer listCodes = new StringBuffer();
        private final Map<Character, Node> mapCharNodes = new HashMap<>();

        public EncodingString(String sourceString) {
            this.sourceString = sourceString;

            createCodesTree(getPriorityQueue(getCharMap()));
        }


        private Map<Character, Integer> getCharMap() {
            Map<Character, Integer> charMap = new HashMap<>();

            for (int i = 0; i < sourceString.length(); i++) {
                char c = sourceString.charAt(i);

                if (charMap.containsKey(c)) {
                    charMap.put(c, charMap.get(c) + 1);
                } else {
                    charMap.put(c, 1);
                }
            }

            //вывод размера карты
            this.charMapSize = charMap.size();

            return charMap;
        }

        private PriorityQueue<Node> getPriorityQueue(Map<Character, Integer> map) {
            PriorityQueue<Node> priorityQueueNode = new PriorityQueue<>();

            for (Map.Entry<Character, Integer> entry : map.entrySet()) {
                LeafNode leafNode = new LeafNode(entry.getKey(), entry.getValue());
                priorityQueueNode.add(leafNode);
                mapCharNodes.put(entry.getKey(), leafNode);
            }

            return priorityQueueNode;
        }

        private void createCodesTree(PriorityQueue<Node> priorityQueue) {
            int sum = 0;
            while (priorityQueue.size() > 1) {
                Node first = priorityQueue.poll();
                Node second = priorityQueue.poll();

                InternalNode node = new InternalNode(first, second);

                sum += node.frequency;

                priorityQueue.add(node);
            }

            //вывод размера кода
            //если карта символов содержит только 1 символ, то выводим длину входной строки, иначе - сумму длин кодов
            this.codeSize = (charMapSize == 1) ? sourceString.length() : sum;

            buildCode(priorityQueue);
        }

        private void buildCode(PriorityQueue<Node> priorityQueue) {
            Node root = priorityQueue.poll();

            if (charMapSize == 1) {
                root.code = "0";
                root.buildCode("0");
            } else {
                root.buildCode("");
            }
        }

        public String getEncodeString() {
            StringBuilder stringResult = new StringBuilder();

            for (int i = 0; i < sourceString.length(); i++) {
                stringResult.append(mapCharNodes.get(sourceString.charAt(i)).code);
            }

            return stringResult.toString();
        }

        @Override
        public String toString() {
            return charMapSize + " " + codeSize + " " +
                    listCodes + getEncodeString();
        }

        private static class Node implements Comparable<Node> {
            final int frequency;
            String code;

            public Node(int frequency) {
                this.frequency = frequency;
            }

            void buildCode(String code) {
                this.code = code;
            }

            @Override
            public int compareTo(Node o) {
                return Integer.compare(frequency, o.frequency);
            }
        }

        private static class InternalNode extends Node {
            Node left;
            Node right;

            public InternalNode(Node left, Node right) {
                super(left.frequency + right.frequency);
                this.left = left;
                this.right = right;
            }

            @Override
            void buildCode(String code) {
                super.buildCode(code);
                left.buildCode(code + "0");
                right.buildCode(code + "1");
            }
        }

        private class LeafNode extends Node {
            char symbol;

            public LeafNode(char symbol, int count) {
                super(count);
                this.symbol = symbol;
            }

            @Override
            void buildCode(String code) {
                super.buildCode(code);
                listCodes.append(symbol).append(":").append(code).append(" ");
            }
        }
    }
}
