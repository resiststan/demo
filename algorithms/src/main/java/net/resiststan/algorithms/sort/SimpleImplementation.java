package net.resiststan.algorithms.sort;

import java.util.Arrays;

public class SimpleImplementation {
    private static int min;
    private static int max;
    private static int dif;

    /**
     * Сортировка с помощью двоичного дерева
     * <p>
     * Худшее время 	O()
     * Лучшее время 	O()
     * Среднее время 	O()
     * Затраты памяти 	O()
     */
    public static void binTree(int[] a) {
        class Tree {
            private int j = 0;

            class Node {
                public Node left;
                public Node right;
                public int key;

                public Node(int k) {
                    key = k;
                }

                /*
                    key >= node.key - рекурсивно добавить новый узел в правое
                    key < node.key - рекурсивно добавить новый узел в левое
                    вставить на это место - если узла нет
                */
                public void insert(Node node) {
                    if (node.key < key) {
                        if (left != null) {
                            left.insert(node);
                        } else {
                            left = node;
                        }
                    } else if (right != null) {
                        right.insert(node);
                    } else {
                        right = node;
                    }
                }

                //рекурсивный обход, сперва - лево, потом право
                public void walk() {
                    if (left != null) {
                        left.walk();
                    }

                    pop(this);

                    if (right != null) {
                        right.walk();
                    }
                }

                public void pop(Node node) {
                    a[j++] = node.key;
                }
            }
        }

        Tree tree = new Tree();
        Tree.Node node = tree.new Node(a[0]);
        for (int i = 1; i < a.length; i++) {
            node.insert(tree.new Node(a[i]));
        }
        node.walk();
    }

    /**
     * Блочная сортировка
     * <p>
     * Худшее время 	O(n2)
     * Лучшее время 	O(n * k)
     * Среднее время 	O(n + n2 / k + k)
     * Затраты памяти 	O()
     */
    public static void bucket(int[] a) {
        setMinMax(a);
        correctMinMax();

        int[] bucket = new int[max + 1];

        Arrays.fill(bucket, 0);

        for (int i = 0; i < a.length; i++) {
            bucket[a[i] + dif]++;
        }

        int outP = 0;

        for (int i = 0; i < bucket.length; i++) {
            for (int j = 0; j < bucket[i]; j++) {
                a[outP++] = i - dif;
            }
        }
    }

    /**
     * Поразрядная сортировка
     * <p>
     * Худшее время 	O()
     * Лучшее время 	O()
     * Среднее время 	O(nk)
     * Затраты памяти 	O(k)
     */
    public static void radix(int[] a) {
        setMinMax(a);
        correctMinMax();

        for (int exp = 1; max / exp > 0; exp *= 10) {
            countingSort(a, a.length, exp);
        }
    }

    private static void countingSort(int[] a, int n, int exp) {
        int[] output = new int[n];
        int[] count = new int[10];
        Arrays.fill(count, 0);

        for (int i = 0; i < n; i++) {
            count[((a[i] + dif) / exp) % 10]++;
        }

        for (int i = 1; i < 10; i++) {
            count[i] += count[i - 1];
        }

        for (int i = n - 1; i >= 0; i--) {
            output[count[((a[i] + dif) / exp) % 10] - 1] = a[i] + dif;
            count[((a[i] + dif) / exp) % 10]--;
        }

        for (int i = 0; i < n; i++) {
            a[i] = output[i] - dif;
        }
    }

    /**
     * Сортировка чёт-нечет
     * <p>
     * Худшее время 	O()
     * Лучшее время 	O()
     * Среднее время 	O()
     * Затраты памяти 	O()
     */
    public static void oddEven(int[] a) {
        for (int i = 0; i < a.length; i++) {
            for (int j = (i % 2); j < a.length - 1; j += 2) {
                if (a[j] > a[j + 1]) {
                    swap(a, j, j + 1);
                }
            }
        }
    }

    /**
     * Сортировка расчёской
     * <p>
     * Худшее время 	O(n^2)
     * Лучшее время 	O(n log n)
     * Среднее время 	O()
     * Затраты памяти 	O(1)
     */
    public static void comb(int[] a) {
        final double CONST = 1.247330950103979;

        int gap = a.length;
        boolean swapped = true;

        while (gap > 1 || swapped) {
            if (gap > 1) {
                gap = (int) (gap / CONST);
            }

            int i = 0;
            swapped = false;

            while (i + gap < a.length) {
                if (a[i] > (a[i + gap])) {
                    int tmp = a[i];
                    a[i] = a[i + gap];
                    a[i + gap] = tmp;

                    swapped = true;
                }

                i++;
            }
        }
    }

    /**
     * Сортировка перемешиванием
     * <p>
     * Худшее время 	O(n2)
     * Лучшее время 	O(n)
     * Среднее время 	O(n2)
     * Затраты памяти 	O(1)
     */
    public static void cocktail(int[] a) {
        int l = 0;
        int r = a.length - 1;

        do {
            for (int i = l; i < r; i++) {
                if (a[i] > a[i + 1]) {
                    swap(a, i, i + 1);
                }
            }

            r--;

            for (int i = r; i > l; i--) {
                if (a[i] < a[i - 1]) {
                    swap(a, i, i - 1);
                }
            }

            l++;
        } while (l < r);
    }

    /**
     * Сортировка слиянием
     * <p>
     * Худшее время 	O(n log n)
     * Лучшее время 	O(n log n)
     * Среднее время 	O(n log n)
     * Затраты памяти 	O(n) вспомогательных
     */
    public static void merge(int[] a) {
        if (a.length > 1) {
            int mid = a.length / 2;
            int[] left = Arrays.copyOfRange(a, 0, mid);
////equals
//            int[] left = new int[mid];
//            for (int i = 0; i < left.length; i++) {
//                left[i] = a[i];
//            }

            int[] right = Arrays.copyOfRange(a, mid, a.length);
////equals
//            int[] right = new int[a.length - mid];
//            for (int i = 0; i < right.length; i++) {
//                right[i] = a[i + mid];
//            }

            merge(left);
            merge(right);
            mergeArrays(left, right, a);
        }
    }

    private static void mergeArrays(int[] left, int[] right, int[] result) {
        int iL = 0;
        int iR = 0;

        for (int i = 0; i < result.length; i++) {
            if (iR >= right.length || (iL < left.length && left[iL] <= right[iR])) {
                result[i] = left[iL++];
            } else {
                result[i] = right[iR++];
            }
        }

    }

    /**
     * Худшее время 	O(n2)
     * Лучшее время 	O(n log2 n)
     * Среднее время 	зависит от выбранных шагов
     * Затраты памяти 	О(n) всего, O(1) дополнительно
     * <p>
     * является улучшенным вариантом сортировки вставками
     */
    public static void shell(int[] a) {
        int increment = a.length / 2;

        while (increment >= 1) {
            for (int i = 0; i < increment; i++) {
                doInsertionSort(a, i, increment);
            }

            increment /= 2;
        }
    }

    private static void doInsertionSort(int[] a, int startIndex, int increment) {
        for (int i = startIndex; i < a.length - 1; i += increment) {
            for (int j = Math.min(i + increment, a.length - 1); j - increment >= 0; j -= increment) {
                if (a[j - increment] > a[j]) {
                    int tmp = a[j];
                    a[j] = a[j - increment];
                    a[j - increment] = tmp;
                } else {
                    break;
                }
            }
        }
    }

    /**
     * Худшее время 	О(n2) сравнений, обменов
     * Лучшее время 	O(n) сравнений, 0 обменов
     * Среднее время 	О(n2) сравнений, обменов
     * Затраты памяти 	О(n) всего, O(1) вспомогательный
     */
    public static void insertion(int[] a) {
        doInsertionSort(a, 0, 1);
    }


    /**
     * Худшее время 	О(n2) сравнений, обменов
     * Лучшее время 	O(n) сравнений, 0 обменов
     * Среднее время 	О(n2) сравнений, обменов
     * Затраты памяти 	О(1)
     */
    public static void gnome(int[] a) {
        int i = 1;

        while (i < a.length) {
            if (i == 0 || a[i - 1] <= a[i]) {
                i++;
            } else {
                swap(a, i, i - 1);

                i--;
            }
        }
    }

    /**
     * Худшее время 	O(n2)
     * Лучшее время 	O(n log n) (обычное разделение) или O(n) (разделение на 3 части)
     * Среднее время 	O(n log n)
     * Затраты памяти 	O(n) вспомогательных, O(log n) вспомогательных
     */
    public static void quick(int[] a) {
        int low = 0;
        int high = a.length - 1;
        doQuickSort(a, low, high);
    }

    private static void doQuickSort(int[] a, int low, int high) {
        if (a.length < 2 || low >= high) {
            return;
        }

        //выбор опорного элемента
        int mid = low + (high - low) / 2;
        int o = a[mid];

        //разделение на подмассивы
        int i = low, j = high;
        while (i <= j) {
            while (a[i] < o) {
                i++;
            }

            while (a[j] > o) {
                j--;
            }

            if (i <= j) {
                int tmp = a[i];
                a[i++] = a[j];
                a[j--] = tmp;
            }
        }

        //рекурсия для сортировки частей
        if (low < j) {
            doQuickSort(a, low, j);
        }

        if (high > i) {
            doQuickSort(a, i, high);
        }
    }

    /**
     * Худшее время 	O(n+k)
     * Лучшее время     O(2n)
     * Среднее время    -//-
     * Затраты памяти 	O(n+k)
     */
    public static void byCounting(int[] a) {
        setMinMax(a);
        correctMinMax();

        int[] tmpA = new int[max + 1];

        for (int i = 0; i < a.length; i++) {
            tmpA[a[i] + dif]++;
        }

        int j = 0;
        for (int i = 0; i < tmpA.length; i++) {
            while (tmpA[i]-- > 0) {
                a[j++] = i - dif;
            }
        }
    }

    /**
     * Худшее время 	O(n2)
     * Лучшее время     O(n)
     * Среднее время    -
     * Затраты памяти 	-
     */
    public static void bubble(int[] a) {
        for (int i = a.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (a[j] > a[j + 1]) {
                    swap(a, j, j + 1);
                }
            }
        }
    }

    private static void setMinMax(int[] a) {
        max = 0;
        min = 0;

        if (a.length > 1) {
            for (int i = 0; i < a.length; i++) {
                if (a[i] >= max) {
                    max = a[i];
                }

                if (a[i] <= min) {
                    min = a[i];
                }
            }
        } else {
            max = a[0];
            min = a[0];
        }
    }

    private static void swap(int[] arr, int i1, int i2) {
        int tmp = arr[i1];
        arr[i1] = arr[i2];
        arr[i2] = tmp;
    }

    private static void correctMinMax() {
        dif = 0;

        if (min < 0) {
            dif = -min;
            min += dif;
            max += dif;
        }
    }
}
